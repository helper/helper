
/*****************************************************************************
FILE		: capturethread.cpp
DESCRIPTION	: Class implementing the capture thread.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "capturethread.h"

#include "../core.h"
#include "../session/session.h"
#include "../globaldefinitions.h"
#include "../session/sessioncommunicator.h"
#include "../session/statemachine/sessionthread.h"

#include <QBuffer>

////////////////////////////////////////////////////////////////////////////////

#define	SYNC	 QMutexLocker	locker( &m_mutex )

////////////////////////////////////////////////////////////////////////////////

CaptureThread::CaptureThread( int screenWidth, int screenHeight ) : QThread(),
    m_buffer( NULL ),
    m_capturer( NULL ),
	m_running( false ),
	m_firstCapture( true ),
	m_canCapture( true )
{
    m_capturer	= new Capturer();
	
	init( screenWidth, screenHeight );
}
////////////////////////////////////////////////////////////////////////////////

CaptureThread::~CaptureThread()
{
    if( m_capturer != NULL )
    {
		delete	m_capturer;
		m_capturer  = NULL;
    }
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::run()
{
	m_running	= true;

    while( m_running )
    {
		SYNC;
		//if( m_canCapture )
		{
			m_capturer->captureDesktop( (char*)m_image.bits() );
			//m_canCapture	= false;

			if( ! m_firstCapture )
			{
				//QImage	screen		= QImage( m_width, m_height, QImage::Format_RGB32 );

				//memcpy( screen.bits(), (unsigned char*)m_buffer, m_width*m_height*4 );

				////	Give the screen to the session thread.
				//g_Core->getSessionThread()->getSession()->setScreen( screen );

				handleScreenUpdates();
				emit	newScreenUpdate();
				m_canCapture	= true;
			}
			else
			{
				handleFirstScreen();
			}
			m_firstCapture	= false;
		}
    }
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::handleFirstScreen()
{
	int			imageByteCount	= m_image.numBytes();
	QByteArray	compressed		= qCompress( m_image.bits(), imageByteCount, 5 );
	int	compressedDataSize		= compressed.size();
	
	qDebug() << "Image numBytes > " << imageByteCount;

	QBuffer		buffer( &compressed );
	buffer.open( QBuffer::ReadOnly );
	buffer.seek( 0 );
	
	SessionCommunicator*	communicator	= g_Core->getSessionThread()->getCommunicator();

	QByteArray	screenInfo;
	
	screenInfo[0]	= (quint16)PACKET_SCREEN_INFO;
	screenInfo[1]	= (quint16)PACKET_SCREEN_INFO	>> 8;
	
	screenInfo[2]	= (quint16)8;
	screenInfo[3]	= (quint16)8	>> 8;

	screenInfo[4]	= (quint16)m_width;
	screenInfo[5]	= (quint16)m_width	>> 8;
	
	screenInfo[6]	= (quint16)m_height;
	screenInfo[7]	= (quint16)m_height	>> 8;
	
	screenInfo[8]	= compressedDataSize;
	screenInfo[9]	= compressedDataSize	>> 8;
	screenInfo[10]	= compressedDataSize	>> 16;
	screenInfo[11]	= compressedDataSize	>> 24;

	qDebug() << "COMPRESSED BYTES SIZE > " << compressedDataSize;

	communicator->pushData( screenInfo );

	int		counter		= 0;
	int		bufferSize	= buffer.size();
	int		bytesToRead	= 1000;


	while( counter < bufferSize )
	{
		if( (bufferSize - counter) < bytesToRead )
			bytesToRead	= bufferSize - counter;

		QByteArray	chunk;
		
		chunk[0]	= (quint16)PACKET_SCREEN_CHUNK;
		chunk[1]	= (quint16)PACKET_SCREEN_CHUNK	>> 8;
	
		chunk[2]	= (quint16)bytesToRead;
		chunk[3]	= (quint16)bytesToRead	>> 8;

		chunk.append( buffer.read(bytesToRead) );

		communicator->pushData( chunk );

		counter	+= bytesToRead;
		buffer.seek( counter );
	}
	
	m_oldImage	= m_image;
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::handleScreenUpdates()
{
	int	columns	= m_image.width()	/ BOX_WIDTH;
	int	rows	= m_image.height()	/ BOX_HEIGHT;	
	
	int	lastColumnWidth	= m_image.width()	% BOX_WIDTH;
	int	lastRowHeight	= m_image.height()	% BOX_HEIGHT;

	if( lastColumnWidth > 0 )
		columns++;
	if( lastRowHeight > 0 )
		rows++;

	Session*	session		= g_Core->getSessionThread()->getSession();

	QList<QByteArray> updates;

	for( int i = 0; i < rows; ++i )
	{
		for( int j = 0; j < columns; ++j )
		{
			QImage	newBox	= m_image.copy( j*BOX_WIDTH, i*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT );
			QImage	oldBox	= m_oldImage.copy( j*BOX_WIDTH, i*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT );
			bool	equal	= memcmp( newBox.bits(), oldBox.bits(), BOX_WIDTH*BOX_HEIGHT*4 ) == 0 ? true : false;

			if( ! equal )
			{
				QByteArray	update;
				QByteArray	data;
				data.resize( newBox.byteCount() );
				memcpy( data.data(), newBox.bits(), newBox.byteCount() );
				
				quint16	xPos	= (quint16)j*BOX_WIDTH;
				quint16	yPos	= (quint16)i*BOX_HEIGHT;
				
				update[0]	= (quint16)PACKET_SCREEN_CHUNK;
				update[1]	= (quint16)PACKET_SCREEN_CHUNK	>> 8;

				update[2]	= (quint16)(data.size() + 8);
				update[3]	= (quint16)(data.size() + 8)	>> 8;

				update[4]	= (quint16)MIDDLE_PACKET;
				update[5]	= (quint16)MIDDLE_PACKET	>> 8;

				update[6]	= xPos;
				update[7]	= xPos	>> 8;

				update[8]	= yPos;
				update[9]	= yPos	>> 8;

				update[10]	= (quint16)data.size();
				update[11]	= (quint16)data.size()	>> 8;

				update.append( data );
				qDebug() << "APPENDING UPDATE( " << xPos << ", " << yPos << ") > " << update.size();
				
				updates.append( update );
			}
		}
	}

	int	updatesCount	= updates.size();
	
	if( updatesCount == 0 )
	{
		/*m_image		=	QImage();*/
		//m_oldImage->fill( Qt::green );
//		g_Core->getCaptureThread()->reinit();
		/*onSendUpdates();*/
		return;
	}
	//	Set FIRST PACKET
	QByteArray	update	= updates[0];
			
	update[4]	= (quint16)FIRST_PACKET;
	update[5]	= (quint16)FIRST_PACKET	>> 8;

	updates[0] = update;

	//	Set LAST PACKET
	update		= updates[updatesCount-1];
	update[4]	= (quint16)LAST_PACKET;
	update[5]	= (quint16)LAST_PACKET	>> 8;

	updates[updatesCount-1] = update;

	qDebug() << "UPDATES COUNT IS : " << updatesCount;

	session->pushUpdates( updates );

	m_oldImage	= m_image;
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::init( int screenW, int screenH )
{
   //m_buffer = (char*)realloc( m_buffer, screenW*screenH*4 );
	m_image		= QImage( screenW, screenH, QImage::Format_RGB32 );
	m_oldImage	= QImage( screenW, screenH, QImage::Format_RGB32 );

   m_width  = screenW;
   m_height = screenH;
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::captureScreen( char* buffer )
{
	SYNC;
	memcpy( buffer, m_buffer, m_width*m_height*4 );
	m_canCapture	= true;
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::stopCapture()
{
	m_running	= false;
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::reinit()
{
	memset( m_buffer, 0, m_width*m_height*4 );

	init( m_width, m_height );
}
////////////////////////////////////////////////////////////////////////////////