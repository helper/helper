
#ifndef __CAPTURETHREAD_H__
#define __CAPTURETHREAD_H__

/*****************************************************************************
FILE		: capturethread.h
DESCRIPTION	: Class implementing the capture thread.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <QImage>
#include <QMutexLocker>
#include <QThread>

#include "../../utilities/Screen/capturer.h"

////////////////////////////////////////////////////////////////////////////////

class CaptureThread : public QThread
{
	Q_OBJECT

public:
    CaptureThread( int screenWidth, int screenHeight );
    ~CaptureThread();

    void    init( int screenW, int screenH );
    void    reinit();
    void    captureScreen( char* buffer );
    void    stopCapture();

signals:
	void	newScreenUpdate();

protected:
    void    run();

private:
	void	handleFirstScreen();
	void	handleScreenUpdates();

private:
	bool		m_running;
	bool		m_canCapture;
	bool		m_firstCapture;

    char*	m_buffer;
    QImage	m_image;
	QImage	m_oldImage;

    Capturer*	m_capturer;

    QMutex	m_mutex;
    int		m_width;
    int		m_height;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __CAPTURETHREAD_H__
