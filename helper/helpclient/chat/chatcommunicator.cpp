////////////////////////////////////////////////////////////////////////////////

#include "chatcommunicator.h"

#include "../core.h"
#include "../session/session.h"
#include "../globaldefinitions.h"
#include "statemachine/chatstateconnect.h"
#include "../session/sessioncommunicator.h"
#include "statemachine/chatstateinitialize.h"
#include "../session/statemachine/sessionthread.h"

#include <QBuffer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QNetworkInterface>

////////////////////////////////////////////////////////////////////////////////

ChatCommunicator::ChatCommunicator() : QObject(),
	m_server( NULL ),
	m_socket( NULL ),
	m_state( NULL ),
	m_connected( false )
{

}
////////////////////////////////////////////////////////////////////////////////

ChatCommunicator::~ChatCommunicator()
{
	if( m_socket != NULL )
	{
		delete	m_socket;
		m_socket	= NULL;
	}

	if( m_server != NULL )
	{
		delete	m_server;
		m_server	= NULL;
	}

	if( m_state != NULL )
	{
		delete	m_state;
		m_state	= NULL;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	ChatCommunicator::start()
{
	changeState( ChatStateInitialize::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	ChatCommunicator::onReadyRead()
{
	qDebug() << "CHAT READY READ";
	m_inputData.append( m_socket->readAll() );
	
	int	totalData	= m_inputData.size();

	if(  totalData >= 4 )
	{
		quint16	displayNameLength	= -1;
		quint16	messageLength		= -1;

		char*	displayNameLen	= (char*)&displayNameLength;
		char*	messageLen		= (char*)&messageLength;

		displayNameLen[0]		= m_inputData[0];
		displayNameLen[1]		= m_inputData[1];

		messageLen[0]	= m_inputData[2];
		messageLen[1]	= m_inputData[3];

		if( totalData < (displayNameLength + messageLength)  )
			return;
		else
		{
			QBuffer	data( &m_inputData );
			data.open( QIODevice::ReadOnly );
			data.seek( 4 );
			
			QString	displayName	= QString::fromUtf8( data.read( displayNameLength ).constData() );
			data.seek( 4 + displayNameLength );

			QString	message		= QString::fromUtf8( data.read( messageLength ).constData() );

			qDebug() << "RECEIVED NEW CHAT MESSAGE FROM > " << displayName << " : " << message;

			data.close();

			m_inputData.remove( 0, 4 + displayNameLength + messageLength );
			
			emit onNewMessage( displayName, message );
		}
	}
}
////////////////////////////////////////////////////////////////////////////////

void	ChatCommunicator::onSendMessage( const QString& displayName, const QString& message )
{
	sendMessage( displayName, message );
}
////////////////////////////////////////////////////////////////////////////////

void	ChatCommunicator::onInitialize()
{
	if( m_server == NULL )
		m_server	= new QTcpServer();
	else
		m_server->close();

	QHostAddress address;

		foreach( QNetworkInterface interf, QNetworkInterface::allInterfaces() )
			if( interf.flags().testFlag(QNetworkInterface::IsUp) && ( ! interf.flags().testFlag(QNetworkInterface::IsLoopBack)) )
				foreach( QNetworkAddressEntry entry, interf.addressEntries() )
				   if( entry.ip().protocol() == QAbstractSocket::IPv4Protocol )
						address	= QHostAddress( entry.ip().toString() );

	m_server->listen( address, CHAT_TCP_LOCAL_PORT );

	if( m_server->isListening() )
		qDebug() << "STARTED LISTENING FOR CHAT MESSAGES > : " << m_server->serverAddress() << ":" << m_server->serverPort();
}
////////////////////////////////////////////////////////////////////////////////

void	ChatCommunicator::onNewConnection()
{
	m_socket	= m_server->nextPendingConnection();
	m_connected	= true;

	qDebug() << "New peer trying to join the chat session : ";
	qDebug() << "Peer IP : " << m_socket->peerAddress();
    qDebug() << "Peer port : " << m_socket->peerPort();
}
////////////////////////////////////////////////////////////////////////////////

void	ChatCommunicator::onConnectToPeer()
{
	QHostAddress	peerAddress	= g_Core->getSessionThread()->getCommunicator()->getPeerAddress();

	if( m_socket != NULL && m_connected )
	{
		m_socket->disconnectFromHost();
		m_socket->connectToHost( peerAddress, CHAT_TCP_LOCAL_PORT );
		return;
	}

	m_socket	= new QTcpSocket();

	ChatStateConnect*	stateConnect	= (ChatStateConnect*)ChatStateConnect::instance();
	connect( m_socket, SIGNAL(connected()),	stateConnect, SLOT(onConnected()) );

	m_socket->connectToHost( peerAddress, CHAT_TCP_LOCAL_PORT );
}
////////////////////////////////////////////////////////////////////////////////

void	ChatCommunicator::sendMessage( const QString& displayName, const QString& message )
{
	QByteArray	data;
	
	QByteArray	myDisplayName;
	myDisplayName.append( displayName.toUtf8().constData() );

	QByteArray	myMessage;
	myMessage.append( message.toUtf8().constData() );

	quint16	messageLength		= (quint16)myMessage.size();
	quint16	displayNameLength	= (quint16)myDisplayName.size();
	
	data[0]	= displayNameLength;
	data[1]	= displayNameLength	>> 8;
	
	data[2]	= messageLength;
	data[3]	= messageLength	>> 8;

	data.append( myDisplayName );
	data.append( myMessage );

	m_socket->write( data );
}
////////////////////////////////////////////////////////////////////////////////