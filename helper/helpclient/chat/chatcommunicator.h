#ifndef __CHATCOMMUNICATOR_H__
#define __CHATCOMMUNICATOR_H__

////////////////////////////////////////////////////////////////////////////////

#include <QObject>
#include <QAbstractSocket>

#include "statemachine/chatsessionstate.h"

////////////////////////////////////////////////////////////////////////////////

class	QTcpServer;
class	QTcpSocket;

////////////////////////////////////////////////////////////////////////////////

class ChatCommunicator : public QObject
{
	Q_OBJECT

public:
	ChatCommunicator();
	~ChatCommunicator();
	
	void	start();
	void	onInitialize();
	void	onNewConnection();
	void	onConnectToPeer();
	void	changeState( ChatSessionState* state );
	void	sendMessage( const QString& displayName, const QString& message );
	
	//	Getters & setters.
	QTcpServer*	getConnectionServer()	const;
	QTcpSocket*	getConnectionSocket()	const;

	bool	isConnected()	const;
	void	setConnected( bool connected );

signals:
	void	onNewMessage( const QString& displayName, const QString& message );

public	slots:
	void	onReadyRead();
	void	onSendMessage( const QString& displayName, const QString& message );

private:
	
	bool		m_connected;
	QByteArray	m_inputData;
	QTcpServer*	m_server;
	QTcpSocket*	m_socket;
	
	ChatSessionState*	m_state;
};

////////////////////////////////////////////////////////////////////////////////

inline
void	ChatCommunicator::changeState( ChatSessionState* state )
{
	m_state	= state;
	m_state->handle();
}
////////////////////////////////////////////////////////////////////////////////

inline
QTcpServer*	ChatCommunicator::getConnectionServer()	const
{
	return	m_server;
}
////////////////////////////////////////////////////////////////////////////////

inline
QTcpSocket*	ChatCommunicator::getConnectionSocket()	const
{
	return	m_socket;
}
////////////////////////////////////////////////////////////////////////////////

inline
bool	ChatCommunicator::isConnected()	const
{
	return	m_connected;
}
////////////////////////////////////////////////////////////////////////////////

inline
void	ChatCommunicator::setConnected( bool connected )
{
	m_connected	= connected;
}
////////////////////////////////////////////////////////////////////////////////

#endif // CHATCOMMUNICATOR_H
