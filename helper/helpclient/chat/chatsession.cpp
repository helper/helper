#include "chatsession.h"

#include "../core.h"
#include "chatcommunicator.h"
#include "../gui/mainwindow.h"
#include "../globaldefinitions.h"

////////////////////////////////////////////////////////////////////////////////

ChatSession::ChatSession( ChatCommunicator& communicator ) : QObject(),
	m_communicator( communicator )
{
}
////////////////////////////////////////////////////////////////////////////////

ChatSession::~ChatSession()
{

}
////////////////////////////////////////////////////////////////////////////////

void	ChatSession::onSendMessage( const QString& displayName, const QString& message )
{
	bool	isConnected	= m_communicator.isConnected();

	//	We are sending the first message.
	if( ! isConnected )
	{
		m_communicator.onConnectToPeer();
		
		m_displayName	= displayName;
		m_messages.append( message );
	}
	else
	{
		m_communicator.sendMessage( displayName, message );
	}
}
////////////////////////////////////////////////////////////////////////////////

void	ChatSession::sendPendingMessages()
{
	int	idx		= 0;
	int	size	= m_messages.size();
	
	while( idx < size )
	{
		m_communicator.sendMessage( m_displayName, m_messages[idx] );
		idx++;
	}
}
////////////////////////////////////////////////////////////////////////////////