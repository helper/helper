#include "chatthread.h"

#include "../core.h"
#include "chatsession.h"
#include "chatcommunicator.h"

////////////////////////////////////////////////////////////////////////////////

ChatThread::ChatThread() : QThread(),
	m_session( NULL ),
	m_communicator( NULL )
{

}
////////////////////////////////////////////////////////////////////////////////

ChatThread::~ChatThread()
{
	if( m_session != NULL )
	{
		delete	m_session;
		m_session	= NULL;
	}

	if( m_communicator != NULL )
	{
		delete	m_communicator;
		m_communicator	= NULL;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	ChatThread::run()
{
	process();
	exec();
}
////////////////////////////////////////////////////////////////////////////////

void	ChatThread::process()
{
	m_communicator	= new ChatCommunicator();
	m_session		= new ChatSession( *m_communicator );
	
	MainWindow*	mainWin	= g_Core->getMainWindow();
	QObject::connect( m_communicator, SIGNAL(onNewMessage(const QString&, const QString&)), mainWin, SLOT(onNewMessage(const QString&, const QString&)), Qt::QueuedConnection );
	QObject::connect( mainWin, SIGNAL(sendMsg(const QString&, const QString&)), m_session,	SLOT(onSendMessage(const QString&, const QString&)), Qt::QueuedConnection );
	
	m_communicator->start();
}
////////////////////////////////////////////////////////////////////////////////