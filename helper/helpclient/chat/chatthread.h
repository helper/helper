#ifndef __CHATTHREAD_H__
#define __CHATTHREAD_H__

#include <QThread>

////////////////////////////////////////////////////////////////////////////////

class	ChatCommunicator;
class	ChatSession;

////////////////////////////////////////////////////////////////////////////////

class ChatThread : public QThread
{
public:
	ChatThread();
	~ChatThread();
	
	ChatSession*		getSession()		const;
	ChatCommunicator*	getCommunicator()	const;

protected:

	virtual	void	run();

private:

	void	process();

private:

	ChatSession*		m_session;		
	ChatCommunicator*	m_communicator;
};
////////////////////////////////////////////////////////////////////////////////

inline
ChatSession*		ChatThread::getSession()		const
{
	return	m_session;
}
////////////////////////////////////////////////////////////////////////////////

inline
ChatCommunicator*	ChatThread::getCommunicator()	const
{
	return	m_communicator;
}
////////////////////////////////////////////////////////////////////////////////

#endif // __CHATTHREAD_H__
