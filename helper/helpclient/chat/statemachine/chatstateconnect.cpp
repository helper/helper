
/*****************************************************************************
FILE		: stateconnected.cpp
DESCRIPTION	: State class used when peer is connected to the host.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "chatstateconnect.h"

#include "../../core.h"
#include "../chatthread.h"
#include "../chatcommunicator.h"
#include "chatstateconnected.h"

#include <QTcpSocket>

////////////////////////////////////////////////////////////////////////////////

ChatStateConnect*    ChatStateConnect::m_instance	= NULL;

////////////////////////////////////////////////////////////////////////////////

ChatStateConnect::ChatStateConnect()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
ChatSessionState*	ChatStateConnect::instance()
{
    if( m_instance == NULL )
		m_instance  = new ChatStateConnect();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	ChatStateConnect::handle()
{
	ChatCommunicator*	communicator	= g_Core->getChatThread()->getCommunicator();
	communicator->onConnectToPeer();
}
////////////////////////////////////////////////////////////////////////////////

void	ChatStateConnect::onConnected()
{
	ChatCommunicator*	communicator	= g_Core->getChatThread()->getCommunicator();
	communicator->changeState( ChatStateConnected::instance() );
}
////////////////////////////////////////////////////////////////////////////////