
/*****************************************************************************
FILE		: stateconnected.cpp
DESCRIPTION	: State class used when peer is connected to the host.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "chatstateconnected.h"

#include "../../core.h"
#include "../chatthread.h"
#include "../chatsession.h"
#include "../chatcommunicator.h"

#include <QTcpSocket>

////////////////////////////////////////////////////////////////////////////////

ChatStateConnected*    ChatStateConnected::m_instance	= NULL;

////////////////////////////////////////////////////////////////////////////////

ChatStateConnected::ChatStateConnected()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
ChatSessionState*	ChatStateConnected::instance()
{
    if( m_instance == NULL )
		m_instance  = new ChatStateConnected();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	ChatStateConnected::onDisconnected()
{
	ChatCommunicator*	communicator	= g_Core->getChatThread()->getCommunicator();
	communicator->setConnected( false );
}
////////////////////////////////////////////////////////////////////////////////

void	ChatStateConnected::onReadyRead()
{
	ChatCommunicator*	communicator	= g_Core->getChatThread()->getCommunicator();
	communicator->onReadyRead();
}
////////////////////////////////////////////////////////////////////////////////

void	ChatStateConnected::onError( QAbstractSocket::SocketError socketError )
{
	ChatCommunicator*	communicator	= g_Core->getChatThread()->getCommunicator();
	communicator->setConnected( false );

	qDebug() << "CHAT COMMUNICATOR ERROR > " << socketError;
}
////////////////////////////////////////////////////////////////////////////////

void	ChatStateConnected::handle()
{
	ChatCommunicator*	communicator	= g_Core->getChatThread()->getCommunicator();
	communicator->setConnected( true );

	QTcpSocket*			socket			= communicator->getConnectionSocket();

	connect( socket, SIGNAL(disconnected()),	this, SLOT(onDisconnected()) );
	connect( socket, SIGNAL(readyRead()),		this, SLOT(onReadyRead()) );
	connect( socket, SIGNAL(error(QAbstractSocket::SocketError)),	SLOT(onError(QAbstractSocket::SocketError)) );

	ChatSession*		session			= g_Core->getChatThread()->getSession();
	session->sendPendingMessages();
}
////////////////////////////////////////////////////////////////////////////////
