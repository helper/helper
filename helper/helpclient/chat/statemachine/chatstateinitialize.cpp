
/*****************************************************************************
FILE		: stateinitialize.cpp
DESCRIPTION	: State class for initializing session state.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "chatstateinitialize.h"

#include "../../core.h"
#include "../chatthread.h"
#include "../chatsession.h"
#include "../chatcommunicator.h"

#include "chatstateconnect.h"
#include "chatstatenewconnection.h"

#include <QTcpServer>

////////////////////////////////////////////////////////////////////////////////

ChatStateInitialize*    ChatStateInitialize::m_instance	= NULL;

////////////////////////////////////////////////////////////////////////////////

ChatStateInitialize::ChatStateInitialize()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
ChatSessionState*	ChatStateInitialize::instance()
{
    if( m_instance == NULL )
		m_instance  = new ChatStateInitialize();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	ChatStateInitialize::handle()
{
	//	Initialize listen for new messages.
	ChatCommunicator*	communicator	= g_Core->getChatThread()->getCommunicator();
	communicator->onInitialize();

	//	Handle incoming connections from peer(s).
	QTcpServer*			server			= communicator->getConnectionServer();
	connect( server,SIGNAL(newConnection()), this, SLOT(onNewConnection()) );

	//	Handle outgoing connections to peer(s).
	/*ChatSession*	session	= g_Core->getChatThread()->getSession();
	connect( session, SIGNAL(connectToPeerChat()), this, SLOT(onConnectToPeer()), Qt::QueuedConnection );*/
}
////////////////////////////////////////////////////////////////////////////////

void	ChatStateInitialize::onNewConnection()
{
	ChatCommunicator*	communicator	= g_Core->getChatThread()->getCommunicator();
	communicator->changeState( ChatStateNewConnection::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	ChatStateInitialize::onConnectToPeer()
{
	ChatCommunicator*	communicator	= g_Core->getChatThread()->getCommunicator();
	communicator->changeState( ChatStateConnect::instance() );
}
////////////////////////////////////////////////////////////////////////////////