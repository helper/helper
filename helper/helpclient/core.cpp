
/*****************************************************************************
FILE		: core.cpp
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "core.h"

#include <QDesktopWidget>

#include "chat/chatthread.h"
#include "session/session.h"
#include "gui/viewerdialog.h"
#include "capture/capturethread.h"
#include "filetransfer/filetransferthread.h"
#include "session/statemachine/sessionthread.h"

#include <QFile>

////////////////////////////////////////////////////////////////////////////////

Core*	Core::m_instance  = NULL;

////////////////////////////////////////////////////////////////////////////////

Core::Core( int argc, char *argv[] ) : m_sessionThread( NULL ),
	m_captureThread( NULL ),
    m_viewerDlg( NULL ),
	m_ftThread( NULL )
{
    m_application   = new QApplication( argc, &*argv );

	QFile qss( ":/styles/style.css" );
	qss.open( QIODevice::ReadOnly );

	if( qss.isOpen() )
		qApp->setStyleSheet( qss.readAll() );

    if( m_instance != NULL )
	return;

    m_instance	    = this;
    m_mainWindow    = new MainWindow();
}
////////////////////////////////////////////////////////////////////////////////

Core::~Core()
{
	if( m_viewerDlg != NULL )
	{
		delete	m_viewerDlg;
		m_viewerDlg	= NULL;
	}
	
	delete	m_mainWindow;
	if( m_captureThread != NULL )
	{
		delete	m_captureThread;
		m_captureThread = NULL;
	}
	if( m_ftThread != NULL )
	{
	    delete	m_ftThread;
		m_ftThread	= NULL;
	}
}
////////////////////////////////////////////////////////////////////////////////

int Core::exec()
{
    return  m_application->exec();
}
////////////////////////////////////////////////////////////////////////////////

void	Core::initialize()
{
    m_mainWindow->show();
}
////////////////////////////////////////////////////////////////////////////////

void	Core::startSession( bool isHost, const QHostAddress& hostAddress )
{
	QDesktopWidget*  desktop = qApp->desktop();

	int	    width   = desktop->width();
	int	    height  = desktop->height();

	if( m_captureThread == NULL && isHost )
	{
		m_captureThread	= new CaptureThread( width, height );
	}

    if( m_sessionThread == NULL )
    {
		m_sessionThread   = new SessionThread( isHost, hostAddress, QSize(width, height) );
		
		if( ! isHost )
			m_viewerDlg		= new ViewerDialog();

		m_sessionThread->start();
    }
}
////////////////////////////////////////////////////////////////////////////////

void	Core::captureDesktop()
{
	m_captureThread->start();
	//	Get	desktop size.
	//QSize	screenSize	= m_sessionThread->getSession()->getScreenSize();

	////	Initialize empty image.
	//QImage	screen		= QImage( screenSize.width(), screenSize.height(), QImage::Format_RGB32 );

	////	Capture the screen by the capture thread.
	//m_captureThread->captureScreen( (char*)screen.bits() );

	////	Give the screen to the session thread.
	//m_sessionThread->getSession()->setScreen( screen );
}
////////////////////////////////////////////////////////////////////////////////

void	Core::onCreateFTSession()
{
	m_ftThread	= new FileTransferThread();
	m_ftThread->start();
}
////////////////////////////////////////////////////////////////////////////////

void	Core::onCreateChatSession()
{
	m_chatThread	= new ChatThread();
	m_chatThread->start();
}
////////////////////////////////////////////////////////////////////////////////
