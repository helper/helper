
#ifndef __CORE_H__
#define __CORE_H__

/*****************************************************************************
FILE		: core.h
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "../gui/mainwindow.h"

#include <QObject>
#include <QHostAddress>
#include <QtGui/QApplication>

////////////////////////////////////////////////////////////////////////////////

#define	g_Core	Core::getInstance()

////////////////////////////////////////////////////////////////////////////////

class	ChatThread;
class	ViewerDialog;
class	QHostAddress;
class	CaptureThread;
class	SessionThread;
class	FileTransferThread;

////////////////////////////////////////////////////////////////////////////////


class Core
{
public:
    Core( int argc, char *argv[] );
    ~Core();

    static  Core*    getInstance() { return m_instance; }

    int	    exec();
    void    initialize();
    void    startSession( bool isHost, const QHostAddress& hostAddress );
	void	captureDesktop();
	
	void	onCreateFTSession();
	void	onCreateChatSession();
	
	ChatThread*		getChatThread()		const;
	QHostAddress	getPeerAddress()	const;
    MainWindow*		getMainWindow()		const;
    ViewerDialog*	getViewerDlg()		const;
    SessionThread*	getSessionThread()	const;
	CaptureThread*	getCaptureThread()	const;
	FileTransferThread*	getFileTransferThread()	const;

private:
    static   Core*   m_instance;

	QHostAddress	m_hostAddress;
    MainWindow*	    m_mainWindow;
    ViewerDialog*   m_viewerDlg;
    QApplication*   m_application;
    SessionThread*  m_sessionThread;
	CaptureThread*	m_captureThread;
	
	ChatThread*			m_chatThread;
	FileTransferThread*	m_ftThread;
};
////////////////////////////////////////////////////////////////////////////////

inline
ChatThread*	Core::getChatThread()	const
{
	return	m_chatThread;
}
////////////////////////////////////////////////////////////////////////////////

inline
QHostAddress	Core::getPeerAddress()	const
{
	return	m_hostAddress;
}
////////////////////////////////////////////////////////////////////////////////

inline
MainWindow* Core::getMainWindow()	const
{
    return  m_mainWindow;
}
////////////////////////////////////////////////////////////////////////////////

inline
ViewerDialog*	Core::getViewerDlg()	const
{
	return	m_viewerDlg;
}
////////////////////////////////////////////////////////////////////////////////

inline
SessionThread* Core::getSessionThread()   const
{
    return  m_sessionThread;
}
////////////////////////////////////////////////////////////////////////////////

inline
CaptureThread*	Core::getCaptureThread()	const
{
	return	m_captureThread;
}
////////////////////////////////////////////////////////////////////////////////

inline
FileTransferThread*	Core::getFileTransferThread()	const
{
	return	m_ftThread;
}
////////////////////////////////////////////////////////////////////////////////

#endif // __CORE_H__