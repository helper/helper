#include "filetransfersession.h"

#include "../core.h"
#include "../gui/mainwindow.h"
#include "../globaldefinitions.h"
#include "filetransferthread.h"
#include "ftcommunicator.h"

#include <QDir>
#include <QFile>
#include <QBuffer>

////////////////////////////////////////////////////////////////////////////////

FileTransferSession::FileTransferSession() : QObject(),
	m_sessionFiles( 0 ),
	m_totalSize( 0 ),
	m_receivedChunks( 0 ),
	m_totalProcessed( 0 )
{

}
////////////////////////////////////////////////////////////////////////////////

FileTransferSession::~FileTransferSession()
{

}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferSession::onPrepareFilesForSend()
{
	QStringList	fileNames	= g_Core->getMainWindow()->getFileNames();
	int	filesCount			= fileNames.size();

	for( int i = 0; i < filesCount; ++i )
	{
		QDir	directory	= QDir( fileNames[i] );
		QFile file( directory.absoluteFilePath(fileNames[i]) );
		
		m_fileNames.append( QFileInfo( file ).fileName() );
		m_fileSizes.append( QFileInfo( file ).size() );

		file.open( QIODevice::ReadOnly );

		QByteArray	fileContent	= file.readAll();

		m_filesContents.append( fileContent );
		
		file.close();
	}
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferSession::onFilesDenied()
{
	m_fileNames.clear();
	m_filesContents.clear();
	m_fileSizes.clear();
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferSession::onFilesAccepted()
{
	int		filesCount	= m_filesContents.size();
	quint64	totalSize	= 0;
	
	for( int i = 0; i < m_filesContents.size(); ++i )
		totalSize += m_filesContents[i].size();

	FTCommunicator*	communicator	= g_Core->getFileTransferThread()->getCommunicator();

	QByteArray	ftSessionInfo;

	ftSessionInfo[0]	= PACKET_FT_SESSION_INFO;
	ftSessionInfo[1]	= PACKET_FT_SESSION_INFO >> 8;

	ftSessionInfo[2]	= 10;
	ftSessionInfo[3]	= 10	>> 8;
	
	//	Next 2 bytes show how many files there are in the session.
	ftSessionInfo[4]	= filesCount;
	ftSessionInfo[5]	= filesCount	>> 8;
	
	//	Next 8 bytes show the total size of files to send.
	ftSessionInfo[6]	= totalSize;
	ftSessionInfo[7]	= totalSize		>> 8;
	ftSessionInfo[8]	= totalSize		>> 16;
	ftSessionInfo[9]	= totalSize		>> 24;
	ftSessionInfo[10]	= totalSize		>> 32;
	ftSessionInfo[11]	= totalSize		>> 40;
	ftSessionInfo[12]	= totalSize		>> 48;
	ftSessionInfo[13]	= totalSize		>> 56;

	m_totalSize	= totalSize;

	communicator->pushData( ftSessionInfo );
	communicator->sendDirect();

	for( int i = 0; i < filesCount; ++i )
	{
		int	fileSize	= m_filesContents[i].size();
		int	totalCycles	= fileSize / BYTES_PORTION;
		int	extraCycles	= fileSize % BYTES_PORTION;

		if( extraCycles > 0 )
			totalCycles++;

		QBuffer	tmpBuffer( &m_filesContents[i] );
		tmpBuffer.open( QIODevice::ReadOnly );
		tmpBuffer.seek( 0 );

		QByteArray	fileData;
		fileData[0]	= PACKET_FT_SESSION;
		fileData[1]	= PACKET_FT_SESSION >> 8;

		QByteArray	arrFileName;
		arrFileName.append( m_fileNames[i].toLocal8Bit().constData() );

		quint16		fileNameSize	= (quint16)arrFileName.size();

		QByteArray	firstPacket;
		firstPacket[0]	= FIRST_PACKET;
		firstPacket[1]	= FIRST_PACKET >> 8;

		firstPacket[2]	= fileNameSize;
		firstPacket[3]	= fileNameSize >> 8;

		QByteArray	readData	= tmpBuffer.read(BYTES_PORTION);

		firstPacket.append( arrFileName );
		firstPacket.append( readData );

		quint16	firstDataSize	= (quint16)firstPacket.size();

		fileData[2]	= firstDataSize;
		fileData[3]	= firstDataSize	>> 8;

		fileData.append( firstPacket );

		communicator->pushData( fileData );
		
		int	totalData	= readData.size();

		for( int y = 1; y < totalCycles; ++y )
		{
			tmpBuffer.seek( y * BYTES_PORTION );

			QByteArray	middleData;
			middleData[0]	= PACKET_FT_SESSION;
			middleData[1]	= PACKET_FT_SESSION >> 8;

			QByteArray	dataRead = tmpBuffer.read( BYTES_PORTION );
			quint16		dataSize = (quint16)dataRead.size();
			
			dataSize += 2;

			middleData[2]	= dataSize;
			middleData[3]	= dataSize	>> 8;
			
			middleData[4]	= MIDDLE_PACKET;
			middleData[5]	= MIDDLE_PACKET	>> 8;
			
			middleData.append( dataRead );
			
			totalData += dataRead.size();

			communicator->pushData( middleData );
		}

		//	CREATE PACKET WITH LAST_PACKET FLAG AND SIZE 0.
		
		QByteArray	lastData;
		lastData[0]	= PACKET_FT_SESSION;
		lastData[1]	= PACKET_FT_SESSION >> 8;

		lastData[2]	= 2;
		lastData[3]	= 2	>> 8;
			
		lastData[4]	= LAST_PACKET;
		lastData[5]	= LAST_PACKET	>> 8;

		communicator->pushData( lastData );

		tmpBuffer.close();
	}

	communicator->sendData();

	//	Notify GUI to show progressbar.
	emit filesAccepted();
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferSession::onFTChunk( const QByteArray& chunkData )
{
	quint16	chunkType	= 0;
	char*	type		= (char*)&chunkType;
		
	type[0]	= chunkData[0];
	type[1]	= chunkData[1];

	switch( chunkType )
	{
	case FIRST_PACKET:
		onFirstPacket( QByteArray(chunkData) );
		break;
	case MIDDLE_PACKET:
		onMiddlePacket( QByteArray(chunkData) );
		break;
	case LAST_PACKET:
		onLastPacket();
		break;
	}
	m_receivedChunks++;

	if( m_receivedChunks == FILES_SEND_CHUNK )
	{
		QByteArray	ackData;
		ackData[0]	= PACKET_FT_PERCENTAGE_ACK;
		ackData[1]	= PACKET_FT_PERCENTAGE_ACK >> 8;

		ackData[2]	= 4;
		ackData[3]	= 4	>> 8;

		ackData[4]	= m_totalProcessed;
		ackData[5]	= m_totalProcessed	>> 8;
		ackData[6]	= m_totalProcessed	>> 16;
		ackData[7]	= m_totalProcessed	>> 24;

		FTCommunicator*	communicator	= g_Core->getFileTransferThread()->getCommunicator();
		communicator->pushData( ackData );
		communicator->sendDirect();

		m_receivedChunks	= 0;
		onFTPercentageAck();
	}
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferSession::onFirstPacket( QByteArray& fileData )
{
	QByteArray	fileContent;

	quint16	fileNameSize	= 0;
	char*	fileName		= (char*)&fileNameSize;
		
	fileName[0]	= fileData[2];
	fileName[1]	= fileData[3];
	
	QBuffer	tmpBuffer( &fileData );
	tmpBuffer.open( QIODevice::ReadOnly );
	tmpBuffer.seek( 4 );

	QByteArray	fName	= tmpBuffer.read( fileNameSize );
	QString		nameStr	= QString::fromUtf8( fName.constData() );

	m_inputFileNames.append( nameStr );
	qDebug() << "FILE NAME IS " <<nameStr;

	int	nextPos	= 4 + fileNameSize;
	tmpBuffer.seek( nextPos );

	int	bytesToRead	= fileData.size() - nextPos;

	fileContent.append( tmpBuffer.read(bytesToRead) );

	//	Increase currently received total size.
	m_totalProcessed += fileContent.size();

	m_inputFilesContents.append( fileContent );

	tmpBuffer.close();
	
	//	Check if we have to update GUI.
	if( m_totalProcessed == m_totalSize )
	{
		QByteArray	ackData;
		ackData[0]	= PACKET_FT_PERCENTAGE_ACK;
		ackData[1]	= PACKET_FT_PERCENTAGE_ACK >> 8;

		ackData[2]	= 4;
		ackData[3]	= 4	>> 8;

		ackData[4]	= m_totalProcessed;
		ackData[5]	= m_totalProcessed	>> 8;
		ackData[6]	= m_totalProcessed	>> 16;
		ackData[7]	= m_totalProcessed	>> 24;

		FTCommunicator*	communicator	= g_Core->getFileTransferThread()->getCommunicator();
		communicator->pushData( ackData );
		communicator->sendData();

		m_receivedChunks	= 0;
		onFTPercentageAck();
	}

	qDebug() << "FIRST PACKET OF FILE RECEIVED";
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferSession::onMiddlePacket( QByteArray& fileData )
{
	int	currentFilesCount	= m_inputFilesContents.size();
	int	lastFileContent		= currentFilesCount - 1;
	
	QBuffer	tmpBuffer( &fileData );
	tmpBuffer.open( QIODevice::ReadOnly );
	tmpBuffer.seek( 2 );
	
	m_totalProcessed += fileData.size() - 2;

	m_inputFilesContents[lastFileContent].append( tmpBuffer.read(fileData.size() - 2) );
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferSession::onLastPacket()
{
	//	Check if we have receive all files.
	//	If so, save all files in the folder 'ReceivedFiles'.
	if( m_sessionFiles == m_inputFilesContents.size() )
	{
		saveReceivedFiles();
		
		m_totalProcessed	= m_totalSize;

		//	Notify the sender that the receiver has received all the files.
		{
			QByteArray	ackData;
			ackData[0]	= PACKET_FT_PERCENTAGE_ACK;
			ackData[1]	= PACKET_FT_PERCENTAGE_ACK >> 8;

			ackData[2]	= 4;
			ackData[3]	= 4	>> 8;

			ackData[4]	= m_totalProcessed;
			ackData[5]	= m_totalProcessed	>> 8;
			ackData[6]	= m_totalProcessed	>> 16;
			ackData[7]	= m_totalProcessed	>> 24;

			FTCommunicator*	communicator	= g_Core->getFileTransferThread()->getCommunicator();
			communicator->pushData( ackData );
			communicator->sendData();

			m_receivedChunks	= 0;
		}
		
		//	Update the GUI on receiver's side.
		onFTPercentageAck();
	}
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferSession::saveReceivedFiles()
{
	int filesCount	= m_inputFileNames.size();
	
	//	Check if we have the 'ReceivedFiles' folder.
	//	If not, create it.
	QDir	appDir	= qApp->applicationDirPath();

	if( ! appDir.exists("ReceivedFiles") )
		appDir.mkdir( "ReceivedFiles" );

	for( int i = 0; i < filesCount; ++i )
	{
		QString	fileAbsPath	= QString::fromUtf8( "%1%2%3%4%5" ).arg( appDir.absolutePath() )
		.arg( QDir::separator() ).arg( "ReceivedFiles" ).arg( QDir::separator() )
		.arg( m_inputFileNames[i].toUtf8().constData() );

		QFile	file( fileAbsPath );
		file.open( QIODevice::WriteOnly );

		file.write( m_inputFilesContents[i] );
		file.close();
	}
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferSession::onFTPercentageAck()
{
	quint64	processed	= ( m_totalProcessed * 100 );
	quint64	percentage	= processed / m_totalSize;

	//	Clear session buffers.
	if( percentage == 100 )
	{
		m_fileNames.clear();
		m_filesContents.clear();
		m_fileSizes.clear();
		m_inputFileNames.clear();
		m_inputFilesContents.clear();
		m_inputFileSizes.clear();
		m_receivedChunks	= 0;
		m_sessionFiles		= 0;
		m_totalProcessed	= 0;
		m_totalSize			= 0;
	}

	emit	percentageAcknowledge( percentage );
}
////////////////////////////////////////////////////////////////////////////////