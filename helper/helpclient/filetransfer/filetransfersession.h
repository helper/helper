#ifndef __FILETRANSFERSESSION_H__
#define __FILETRANSFERSESSION_H__

////////////////////////////////////////////////////////////////////////////////

#include <QObject>
#include <QStringList>

////////////////////////////////////////////////////////////////////////////////

class FileTransferSession : public QObject
{
	Q_OBJECT

public:
	FileTransferSession();
	~FileTransferSession();
	
	void	onPrepareFilesForSend();
	void	onFilesDenied();
	void	onFilesAccepted();
	void	onSessionInfo( int	sessionFiles, int totalSize );
	void	onFTChunk( const QByteArray& chunkData );
	void	onFTPercentageAck();
	void	updateTotalProcessed( int updateWith );

	int		getFilesTotalSize();

signals:

	void	filesAccepted();
	void	percentageAcknowledge( int percentage );

private:

	void	onFirstPacket( QByteArray& fileData );
	void	onMiddlePacket( QByteArray& fileData );
	void	onLastPacket();
	void	saveReceivedFiles();

private:
	int					m_receivedChunks;
	int					m_sessionFiles;
	int					m_totalSize;
	int					m_totalPercentage;
	quint64				m_totalProcessed;

	QStringList			m_fileNames;
	QList<quint64>		m_fileSizes;
	QList<QByteArray>	m_filesContents;
	
	QStringList			m_inputFileNames;
	QList<quint64>		m_inputFileSizes;
	QList<QByteArray>	m_inputFilesContents;

};
////////////////////////////////////////////////////////////////////////////////

inline
void	FileTransferSession::onSessionInfo( int	sessionFiles, int totalSize )
{
	m_totalSize		= totalSize;
	m_sessionFiles	= sessionFiles;
}
////////////////////////////////////////////////////////////////////////////////

inline
void	FileTransferSession::updateTotalProcessed( int updateWith )
{
	m_totalProcessed	= updateWith;
}
////////////////////////////////////////////////////////////////////////////////

inline
int	FileTransferSession::getFilesTotalSize()
{
	return	m_totalSize;
}
////////////////////////////////////////////////////////////////////////////////

#endif // __FILETRANSFERSESSION_H__
