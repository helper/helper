#include "filetransferthread.h"

#include "../core.h"
#include "../gui/mainwindow.h"
#include "ftcommunicator.h"
#include "filetransfersession.h"

////////////////////////////////////////////////////////////////////////////////

FileTransferThread::FileTransferThread() : QThread(),
	m_session( NULL ),
	m_communicator( NULL )
{

}
////////////////////////////////////////////////////////////////////////////////

FileTransferThread::~FileTransferThread()
{
	if( m_communicator != NULL )
	{
		delete	m_communicator;
		m_communicator	= NULL;
	}

	if( m_session != NULL )
	{
		delete	m_session;
		m_session	= NULL;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferThread::run()
{
	process();
	exec();
}
////////////////////////////////////////////////////////////////////////////////

void	FileTransferThread::process()
{
	m_session		= new FileTransferSession();
	m_communicator	= new FTCommunicator();
	
	MainWindow*	mainWin	= g_Core->getMainWindow();
	QObject::connect( mainWin, SIGNAL(sendRequest()), m_communicator, SLOT(onSendFTRequest()), Qt::QueuedConnection );
	QObject::connect( mainWin, SIGNAL(denyFiles()), m_communicator, SLOT(onDenyFiles()), Qt::QueuedConnection );
	QObject::connect( mainWin, SIGNAL(acceptFiles()), m_communicator, SLOT(onAcceptFiles()), Qt::QueuedConnection );
	QObject::connect( m_session, SIGNAL(percentageAcknowledge(int)), mainWin, SLOT(onPercentageAcknowledge(int)), Qt::QueuedConnection );
	QObject::connect( m_session, SIGNAL(filesAccepted()), mainWin, SLOT(onFilesAccepted()), Qt::QueuedConnection );
	QObject::connect( m_communicator, SIGNAL(ftRequest()), mainWin, SLOT(onFTRequest()), Qt::QueuedConnection );

	m_communicator->start();
}
////////////////////////////////////////////////////////////////////////////////