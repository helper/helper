#ifndef __FILETRANSFERTHREAD_H__
#define __FILETRANSFERTHREAD_H__

#include <QThread>

////////////////////////////////////////////////////////////////////////////////

class	FTCommunicator;
class	FileTransferSession;

////////////////////////////////////////////////////////////////////////////////

class FileTransferThread : public QThread
{
public:
	FileTransferThread();
	~FileTransferThread();
	
	FTCommunicator*			getCommunicator()	const;
	FileTransferSession*	getSession()		const;

protected:

	virtual	void	run();

private:

	void	process();

private:

	FTCommunicator*			m_communicator;
	FileTransferSession*	m_session;
	
};
////////////////////////////////////////////////////////////////////////////////

inline
FileTransferSession*	FileTransferThread::getSession()	const
{
	return	m_session;
}
////////////////////////////////////////////////////////////////////////////////

inline
FTCommunicator*	FileTransferThread::getCommunicator()	const
{
	return	m_communicator;
}
////////////////////////////////////////////////////////////////////////////////

#endif // __FILETRANSFERTHREAD_H__
