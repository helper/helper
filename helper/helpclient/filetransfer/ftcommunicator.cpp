#include "ftcommunicator.h"

#include "../core.h"
#include "../globaldefinitions.h"
#include "filetransfersession.h"
#include "filetransferthread.h"

#include "statemachine/ftstateconnected.h"
#include "statemachine/ftstateinitialize.h"
#include "statemachine/ftstatepreparefiles.h"
#include "statemachine/ftstatefilesdenied.h"
#include "statemachine/ftstatefilesaccepted.h"
#include "statemachine/ftstatesend.h"

#include "../session/session.h"
#include "../session/sessioncommunicator.h"
#include "../session/statemachine/sessionthread.h"

#include <QBuffer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QNetworkInterface>

////////////////////////////////////////////////////////////////////////////////

FTCommunicator::FTCommunicator() : QObject(),
	m_server( NULL ),
	m_socket( NULL ),
	m_state( NULL )
{

}
////////////////////////////////////////////////////////////////////////////////

FTCommunicator::~FTCommunicator()
{
	if( m_server != NULL )
	{
		delete	m_server;
		m_server	= NULL;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::start()
{
	changeState( FTStateInitialize::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onStart()
{
	m_server	= new QTcpServer();
	connect( m_server,SIGNAL(newConnection()), SLOT(onNewConnection()) );

	QHostAddress address;

		foreach( QNetworkInterface interf, QNetworkInterface::allInterfaces() )
			if( interf.flags().testFlag(QNetworkInterface::IsUp) && ( ! interf.flags().testFlag(QNetworkInterface::IsLoopBack)) )
				foreach( QNetworkAddressEntry entry, interf.addressEntries() )
				   if( entry.ip().protocol() == QAbstractSocket::IPv4Protocol )
						address	= QHostAddress( entry.ip().toString() );

	m_server->listen( address, FT_TCP_LOCAL_PORT );

	if( m_server->isListening() )
		qDebug() << "Started listening for file transfers : " << m_server->serverAddress() << ":" << m_server->serverPort();
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::send()
{
	int	data	= m_data.size();

	int	maxSend	= FILES_SEND_CHUNK;

	if( data < maxSend )
		maxSend	= data;

	int	idx		= 0;

	while( idx < maxSend )
	{
		m_socket->write( m_data[idx] );
		idx++;
	}
	
	idx	= 0;

	while( idx < maxSend )
	{
		m_data.removeAt( 0 );
		idx++;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::sendData()
{
	changeState( FTStateSend::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::sendDirect()
{
	int	data	= m_data.size();
	int	idx		= 0;

	while( idx < data )
	{
		m_socket->write( m_data[idx] );
		idx++;
	}

	m_data.clear();
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onSendFTRequest()
{
	m_socket	= new QTcpSocket();

	connect( m_socket, SIGNAL(connected()),		this, SLOT(onConnected()) );
	connect( m_socket, SIGNAL(disconnected()),	this, SLOT(onDisconnected()) );
	connect( m_socket, SIGNAL(readyRead()),		this, SLOT(onReadyRead()) );
	connect( m_socket, SIGNAL(error(QAbstractSocket::SocketError)),	SLOT(onError(QAbstractSocket::SocketError)) );

	QHostAddress	peerAddress	= g_Core->getSessionThread()->getCommunicator()->getPeerAddress();

	m_socket->connectToHost( peerAddress, FT_TCP_LOCAL_PORT );
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onDenyFiles()
{
	QByteArray	responseData;

	responseData[0]	= PACKET_FT_RESPONSE_DENY;
	responseData[1]	= PACKET_FT_RESPONSE_DENY >> 8;

	responseData[2]	= 0;
	responseData[3]	= 0 >> 8;

	m_data.append( responseData );

	qDebug() << "SENDING DENY FILES RESPONSE > ";
	sendDirect();
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onAcceptFiles()
{
	QByteArray	responseData;

	responseData[0]	= PACKET_FT_RESPONSE_ACCEPT;
	responseData[1]	= PACKET_FT_RESPONSE_ACCEPT >> 8;

	responseData[2]	= 0;
	responseData[3]	= 0 >> 8;

	m_data.append( responseData );

	qDebug() << "SENDING ACCEPT FILES RESPONSE > ";
	sendDirect();
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::pushData( const QByteArray& data )
{
	m_data.append( data );
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::notifyGuiFTRequest()
{
	emit	ftRequest();
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onNewConnection()
{	
	m_socket	= m_server->nextPendingConnection();

	connect( m_socket, SIGNAL(connected()),		this, SLOT(onConnected()) );
	connect( m_socket, SIGNAL(disconnected()),	this, SLOT(onDisconnected()) );
	connect( m_socket, SIGNAL(readyRead()),		this, SLOT(onReadyRead()) );
	connect( m_socket, SIGNAL(error(QAbstractSocket::SocketError)),	SLOT(onError(QAbstractSocket::SocketError)) );

	qDebug() << "New peer trying to join the session : ";
	qDebug() << "Peer IP : " << m_socket->peerAddress();
    qDebug() << "Peer port : " << m_socket->peerPort();

	changeState( FTStateConnected::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onConnected()
{
	qDebug() << "FT CONNECTED > " << m_socket->peerAddress();

	//	Start prepare the files for sending when request is accepted.

	changeState( FTStatePrepareFiles::instance() );

	//changeState( FTStateConnected::instance() );
	
	/*QByteArray	requestData;

	requestData[0]	= PACKET_FT_REQUEST;
	requestData[1]	= PACKET_FT_REQUEST >> 8;

	requestData[2]	= 0;
	requestData[3]	= 0 >> 8;

	m_data.append( requestData );

	qDebug() << "SENDING REQUEST FOR FILE TRANSFER > ";
	send();*/
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onDisconnected()
{
	qDebug() << "FT DISCONNECTED > " << m_socket->peerAddress();
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onReadyRead()
{
	qDebug() << "FT READY READ";
		m_inputData.append( m_socket->readAll() );
	
	int	totalData	= m_inputData.size();

	if(  totalData >= 4 )
	{
		quint16	packetType	= -1;
		quint16	dataSize	= -1;

		char*	packet		= (char*)&packetType;
		char*	inpDataSize	= (char*)&dataSize;

		packet[0]		= m_inputData[0];
		packet[1]		= m_inputData[1];

		inpDataSize[0]	= m_inputData[2];
		inpDataSize[1]	= m_inputData[3];

		if( totalData < dataSize )
			return;

		/*qDebug() << "PACKET TYPE > " << packetType;
		qDebug() << "DATA SIZE > "	 << dataSize;
		qDebug() << "INPUT DATA BUFFER SIZE > "	 << totalData;*/

		switch( packetType )
		{
		case PACKET_FT_REQUEST:
			{
				onRequest();
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );

				//	Can process some more data.
				if( m_inputData.size() > 4 )
					onReadyRead();
			}
			break;
		case PACKET_FT_RESPONSE_DENY:
			{
				onFilesDenied();
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );

				//	Can process some more data.
				if( m_inputData.size() > 4 )
					onReadyRead();
			}
			break;
		case PACKET_FT_RESPONSE_ACCEPT:
			{
				onFilesAccepted();
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );

				//	Can process some more data.
				if( m_inputData.size() > 4 )
					onReadyRead();
			}
			break;
		case PACKET_FT_SESSION_INFO:
			{
				QBuffer	reader( &m_inputData );
				reader.open( QIODevice::ReadOnly );
				reader.seek( 4 );

				QByteArray	sessionInfo	= reader.read( dataSize  );
				onSessionInfo( sessionInfo );

				reader.close();
				
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );
			}
			break;
		case PACKET_FT_SESSION:
			{
				QBuffer	reader( &m_inputData );
				reader.open( QIODevice::ReadOnly );
				reader.seek( 4 );

				QByteArray	chunkData	= reader.read( dataSize  );
				onFTSessionChunk( chunkData );

				reader.close();
				
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );

				//	Can process some more data.
				if( m_inputData.size() > 4 )
					onReadyRead();
			}
			break;
		case PACKET_FT_PERCENTAGE_ACK:
			{
				QBuffer	reader( &m_inputData );
				reader.open( QIODevice::ReadOnly );
				reader.seek( 4 );

				QByteArray	info	= reader.read( dataSize  );
				onFTPercentageAck( info );

				reader.close();
				
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );

				//	Can process some more data.
				if( m_inputData.size() > 4 )
					onReadyRead();
			}
			break;
			default:
				qDebug() << "MESSY";
		}
	}
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onError( QAbstractSocket::SocketError socketError )
{
	qDebug() << "FILE TRANSFER SESSION COMMUNICATOR SOCKET ERROR > " << socketError;
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onRequest()
{
	qDebug() << "INCOMING FT REQUEST";
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onFilesDenied()
{
	changeState( FTStateFilesDenied::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onFilesAccepted()
{
	changeState( FTStateFilesAccepted::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onSessionInfo( const QByteArray& info )
{
	quint16	filesCount	= 0;
	quint64	totalSize	= 0;

	char*	count		= (char*)&filesCount;
	char*	size		= (char*)&totalSize;
		
	count[0]	= info[0];
	count[1]	= info[1];

	size[0]	= info[2];
	size[1]	= info[3];
	size[2]	= info[4];
	size[3]	= info[5];
	size[4]	= info[6];
	size[5]	= info[7];
	size[6]	= info[8];
	size[7]	= info[9];

	qDebug( "FT SESSION INFO RECEIVED > %d FILES WITH SIZE %d" , filesCount, totalSize );

	FileTransferSession*	currentSession	= g_Core->getFileTransferThread()->getSession();
	currentSession->onSessionInfo( filesCount, totalSize );
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onFTSessionChunk( const QByteArray& chunkData )
{
	FileTransferSession*	currentSession	= g_Core->getFileTransferThread()->getSession();
	currentSession->onFTChunk( chunkData );
}
////////////////////////////////////////////////////////////////////////////////

void	FTCommunicator::onFTPercentageAck( const QByteArray& percentageAck )
{
	int		totalProcessed	= 0;
	char*	processed		= (char*)&totalProcessed;
		
	processed[0]	= percentageAck[0];
	processed[1]	= percentageAck[1];
	processed[2]	= percentageAck[2];
	processed[3]	= percentageAck[3];

	FileTransferSession*	currentSession	= g_Core->getFileTransferThread()->getSession();
	currentSession->updateTotalProcessed( totalProcessed ); 
	currentSession->onFTPercentageAck();

	send();
}
////////////////////////////////////////////////////////////////////////////////