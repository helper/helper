#ifndef __FTCOMMUNICATOR_H__
#define __FTCOMMUNICATOR_H__

////////////////////////////////////////////////////////////////////////////////

#include <QObject>
#include <QAbstractSocket>

#include "statemachine/ftsessionstate.h"

////////////////////////////////////////////////////////////////////////////////

class	QTcpServer;
class	QTcpSocket;

////////////////////////////////////////////////////////////////////////////////

class FTCommunicator : public QObject
{
	Q_OBJECT

public:
	FTCommunicator();
	~FTCommunicator();

	void	start();
	void	onStart();

	//	Sends data in portions.
	void	send();
	void	sendData();
	//	Sends data directly to the peer with no delay.
	void	sendDirect();

	void	notifyGuiFTRequest();
	void	pushData( const QByteArray& data );
	
signals:

	void	ftRequest();

private	slots:
	
	void	onNewConnection();
	void	onConnected();
	void	onDisconnected();
	void	onReadyRead();
	void	onError( QAbstractSocket::SocketError socketError );

	void	onSendFTRequest();
	void	onDenyFiles();
	void	onAcceptFiles();

private:

	void	changeState( FTSessionState* state );
	void	onRequest();
	void	onFilesDenied();
	void	onFilesAccepted();
	void	onSessionInfo( const QByteArray& info );
	void	onFTSessionChunk( const QByteArray& chunkData );
	void	onFTPercentageAck( const QByteArray& percentageAck );

private:
	
	QTcpServer*	m_server;
	QTcpSocket*	m_socket;

	FTSessionState*		m_state;
	QByteArray			m_inputData;
	QList<QByteArray>	m_data;
};
////////////////////////////////////////////////////////////////////////////////

inline
void	FTCommunicator::changeState( FTSessionState* state )
{
	m_state	= state;
	m_state->handle();
}
////////////////////////////////////////////////////////////////////////////////
#endif // __FTCOMMUNICATOR_H__
