
/*****************************************************************************
FILE		: stateconnected.cpp
DESCRIPTION	: State class used when peer is connected to the host.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ftstatefilesdenied.h"

#include "../../core.h"
#include "../ftcommunicator.h"
#include "../filetransferthread.h"
#include "../filetransfersession.h"

////////////////////////////////////////////////////////////////////////////////

FTStateFilesDenied*    FTStateFilesDenied::m_instance	= NULL;

////////////////////////////////////////////////////////////////////////////////

FTStateFilesDenied::FTStateFilesDenied()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
FTSessionState*	FTStateFilesDenied::instance()
{
    if( m_instance == NULL )
		m_instance  = new FTStateFilesDenied();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	FTStateFilesDenied::handle()
{
	FileTransferSession*	session	= g_Core->getFileTransferThread()->getSession();
	session->onFilesDenied();
}
////////////////////////////////////////////////////////////////////////////////