
/*****************************************************************************
FILE		: stateinitialize.cpp
DESCRIPTION	: State class for initializing session state.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ftstateinitialize.h"

#include "../../core.h"
#include "../ftcommunicator.h"
#include "../filetransferthread.h"
#include "../filetransfersession.h"

////////////////////////////////////////////////////////////////////////////////

FTStateInitialize*    FTStateInitialize::m_instance	= NULL;

////////////////////////////////////////////////////////////////////////////////

FTStateInitialize::FTStateInitialize()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
FTSessionState*	FTStateInitialize::instance()
{
    if( m_instance == NULL )
		m_instance  = new FTStateInitialize();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	FTStateInitialize::handle()
{
	FTCommunicator*	communicator	= g_Core->getFileTransferThread()->getCommunicator();
	communicator->onStart();
}
////////////////////////////////////////////////////////////////////////////////