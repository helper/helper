
#ifndef __FTSTATEPREPAREFILES_H__
#define __FTSTATEPREPAREFILES_H__

/*****************************************************************************
FILE		: stateconnected.h
DESCRIPTION	: State class used when peer is connected to the host.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ftsessionstate.h"

////////////////////////////////////////////////////////////////////////////////

class FTStatePrepareFiles : public FTSessionState
{
public:
    FTStatePrepareFiles();

    static  FTSessionState*   instance();

protected:
    virtual void    handle();

private:
    static  FTStatePrepareFiles*   m_instance;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __FTSTATEPREPAREFILES_H__
