
#ifndef __GLOBALDEFINITIONS_H__
#define __GLOBALDEFINITIONS_H__

/*****************************************************************************
FILE		: globaldefinitions.h
DESCRIPTION	: Contains global definitions of variables, structures, etc.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#define APP_NAME	"Helper"

////////////////////////////////////////////////////////////////////////////////

#define	TCP_LOCAL_PORT		(unsigned int)5858
#define	FT_TCP_LOCAL_PORT	(unsigned int)5859
#define	CHAT_TCP_LOCAL_PORT	(unsigned int)5857

#define	BOX_WIDTH		64
#define	BOX_HEIGHT		64

//	1	MB	1048576
//	512 KB	524288
#define	BYTES_PORTION	60000
#define	FILES_SEND_CHUNK	10

////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////

enum
{
    PACKET_SCREEN_INFO   = 0,
    PACKET_SCREEN_CHUNK,
	PACKET_GET_UPDATES,
	PACKET_SCREEN_UPDATE,
	PACKET_SIMULATE_EVENT,
	PACKET_SIMULATE_MOUSE_EVENT
};
////////////////////////////////////////////////////////////////////////////////

enum
{
	FIRST_PACKET	= 0,
	MIDDLE_PACKET,
	LAST_PACKET
};
////////////////////////////////////////////////////////////////////////////////

enum
{
	KEY_PRESSED		= 10,
	KEY_RELEASED	= 11,
	MOUSE_LEFT_PRESSED	= 12,
	MOUSE_LEFT_RELEASED	= 13,
	MOUSE_RIGHT_PRESSED	= 14,
	MOUSE_RIGHT_RELEASED= 15,
	MOUSE_MOVE			= 16
};
////////////////////////////////////////////////////////////////////////////////

enum
{
	PACKET_FT_REQUEST	= 0,
	PACKET_FT_RESPONSE_DENY,
	PACKET_FT_RESPONSE_ACCEPT,
	PACKET_FT_SESSION_INFO,
	PACKET_FT_SESSION,
	PACKET_FT_PERCENTAGE_ACK
};
#endif // __GLOBALDEFINITIONS_H__
