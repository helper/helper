
/*****************************************************************************
FILE		: mainwindow.cpp
DESCRIPTION	: Application mainwindow

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "../core.h"
#include "../chat/chatthread.h"
#include "../chat/chatsession.h"
#include "../globaldefinitions.h"
#include "../chat/chatcommunicator.h"
#include "../filetransfer/ftcommunicator.h"
#include "../filetransfer/filetransferthread.h"
#include "../filetransfer/filetransfersession.h"

#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QTableWidget>
#include <QHostAddress>

////////////////////////////////////////////////////////////////////////////////

MainWindow::MainWindow( QWidget* parent ) : QMainWindow( parent ),
    m_ui( new Ui::MainWindow ),
	m_filesTable( NULL ),
	m_displayName( "" )
{
    m_ui->setupUi( this );

    //	Connect signals with slots.
    connect( m_ui->btnStartSession, SIGNAL(clicked()), this, SLOT(onStartSession()) );
    connect( m_ui->btnJoinSession,  SIGNAL(clicked()), this, SLOT(onJoinSession())  );
	connect( m_ui->btnBrowseForFiles, SIGNAL(clicked()),this, SLOT(onBrowseForFiles()) );
	connect( m_ui->btnSendFiles, SIGNAL(clicked()),this, SLOT(onStartSendingFiles()) );
	connect( m_ui->btnAcceptFiles, SIGNAL(clicked()),this, SLOT(onAcceptFiles()) );
	connect( m_ui->btnDenyFiles, SIGNAL(clicked()),this, SLOT(onDenyFiles()) );
	connect( m_ui->btnDisplayName, SIGNAL(clicked()), this, SLOT(onSetDislayName()) );
	connect( m_ui->btnSendMessage, SIGNAL(clicked()), this, SLOT(onSendMessage()) );
	
	m_ui->frmFTProgress->hide();
	m_ui->frmReceiveFiles->hide();
	m_ui->tabWidget->setTabEnabled( 2, false );
	m_ui->tabWidget->setTabEnabled( 3, false );

#ifdef	PLATFORM_WIN32
	QString	name = getenv( "USERNAME" );
	m_ui->txtDisplayName->setText( name );
#endif

	setWindowTitle( APP_NAME );
}
////////////////////////////////////////////////////////////////////////////////

MainWindow::~MainWindow()
{
	if( m_filesTable != NULL )
	{
		delete	m_filesTable;
		m_filesTable	= NULL;
	}

    delete  m_ui;
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onPercentageAcknowledge( int percentage )
{
	m_ui->ftProgress->setValue( percentage );

	if( percentage == 100 )
	{
		m_fileNames.clear();
		m_ui->btnBrowseForFiles->setEnabled( true );
	}
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onNewMessage( const QString& displayName, const QString& message )
{
	m_ui->tabWidget->setCurrentIndex( 3 );
	m_ui->txtMessages->appendPlainText( QString("[%1] : %2").arg(displayName).arg(message) );

	show();
	raise();
	activateWindow();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onStartSession()
{
    g_Core->startSession( true, QHostAddress("") );
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onJoinSession()
{
    g_Core->startSession( false, QHostAddress(m_ui->txtIpAddress->text()) );
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onEnableTabs()
{
	//	We are now connected with the peer.
	//	Do some stuff on the GUI and the other plugins.
	m_ui->btnSendFiles->hide();
	m_ui->tabWidget->setTabEnabled( 2, true );
	m_ui->tabWidget->setTabEnabled( 3, true );

	g_Core->onCreateFTSession();
	g_Core->onCreateChatSession();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onBrowseForFiles()
{
	if( m_filesTable == NULL )
		createFilesTable();

	QStringList fileNames	= QFileDialog::getOpenFileNames( this, "Choose files...", QDir::homePath() );

	if( fileNames.size() <= 0 )
		return;

	m_fileNames.append( fileNames );
	
	showFiles();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onStartSendingFiles()
{
	m_ui->btnSendFiles->hide();

	emit	sendRequest();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onFTRequest()
{
	m_ui->tabWidget->setCurrentIndex( 2 );
	m_ui->frmReceiveFiles->show();

	show();
	raise();
	activateWindow();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onAcceptFiles()
{
	m_ui->frmFTProgress->show();
	m_ui->frmReceiveFiles->hide();
	m_ui->btnBrowseForFiles->setEnabled( false );

	emit	acceptFiles();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onDenyFiles()
{
	m_ui->frmReceiveFiles->hide();
	emit	denyFiles();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onFilesAccepted()
{
	m_ui->frmFTProgress->show();
	m_ui->ftProgress->setValue( 0 );
	m_ui->btnBrowseForFiles->setEnabled( false );
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onSetDislayName()
{
	m_displayName	= m_ui->txtDisplayName->text().toUtf8().constData();
	m_ui->frmDisplayName->hide();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onSendMessage()
{
	if( m_ui->txtDisplayName->text().isEmpty() )
	{
		QMessageBox	messageBox( this );
		messageBox.setText( "����, �������� ������ ���������!" );
		messageBox.setIcon( QMessageBox::Warning );
		messageBox.setModal( true );
		messageBox.show();

		return;
	}
	else
	{
		m_displayName	= m_ui->txtDisplayName->text().toUtf8().constData();
	}

	QString	message	= m_ui->txtSendMessage->toPlainText().toUtf8().constData();

	m_ui->txtSendMessage->clear();
	m_ui->txtMessages->appendPlainText( QString("[%1] : %2").arg(m_displayName).arg(message) );

	emit sendMsg( m_displayName, message );
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::createFilesTable()
{
	m_filesTable	= new QTableWidget( 0, 2, this );
    m_filesTable->setSelectionBehavior( QAbstractItemView::SelectRows );

    QStringList labels;
    labels << tr( "File Name" ) << tr( "Size" );

    m_filesTable->setHorizontalHeaderLabels( labels );
    m_filesTable->verticalHeader()->hide();
    m_filesTable->setShowGrid( false );
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::showFiles()
 {
	 int	filesCount	= m_fileNames.size();

	 for( int i = 0; i < m_filesTable->rowCount(); ++i )
		 m_filesTable->removeRow( i );

	 for( int i = 0; i < filesCount; ++i ) 
	 {
		 QDir	directory	= QDir( m_fileNames[i] );
		 QFile file( directory.absoluteFilePath(m_fileNames[i]) );
         qint64 size = QFileInfo( file ).size();

		 QTableWidgetItem*	fileNameItem	= new QTableWidgetItem( QFileInfo( file ).fileName() );
         fileNameItem->setFlags( fileNameItem->flags() ^ Qt::ItemIsEditable );

         QTableWidgetItem*	sizeItem		= new QTableWidgetItem( tr("%1 KB")
                                              .arg(int((size + 1023) / 1024)));
         sizeItem->setTextAlignment( Qt::AlignRight | Qt::AlignVCenter );
         sizeItem->setFlags( sizeItem->flags() ^ Qt::ItemIsEditable );

         int row = m_filesTable->rowCount();
         m_filesTable->insertRow( row );
         m_filesTable->setItem( row, 0, fileNameItem );
         m_filesTable->setItem( row, 1, sizeItem );
     }

	 m_ui->filesLayout->insertWidget(1, m_filesTable );
	 m_filesTable->show();
	 m_ui->btnSendFiles->show();
}
////////////////////////////////////////////////////////////////////////////////