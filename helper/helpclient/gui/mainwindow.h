
#ifndef __MAINWINDOW_H__
#define __MAINWINDOW_H__

/*****************************************************************************
FILE		: mainwindow.h
DESCRIPTION	: Application mainwindow.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include <QMainWindow>

////////////////////////////////////////////////////////////////////////////////

class	QDir;
class	QTableWidget;

///////////////////////////////////////////////////////////////////////////////

namespace Ui
{
    class MainWindow;
}
////////////////////////////////////////////////////////////////////////////////

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:

    explicit MainWindow( QWidget* parent = 0 );
    ~MainWindow();
	
	QStringList	getFileNames()	const;

signals:
	void	sendRequest();
	void	denyFiles();
	void	acceptFiles();
	void	sendMsg( const QString& displayName, const QString& message );

public slots:
	
	void	onPercentageAcknowledge( int percentage );
	void	onNewMessage( const QString& displayName, const QString& message );

private	slots:

    void    onStartSession();
    void    onJoinSession();
	void	onEnableTabs();
	void	onBrowseForFiles();
	void	onStartSendingFiles();
	void	onFTRequest();
	void	onAcceptFiles();
	void	onDenyFiles();
	void	onFilesAccepted();
	void	onSetDislayName();
	void	onSendMessage();

private:

	void	createFilesTable();
	void	showFiles();

private:

	QString			m_displayName;
	QStringList		m_fileNames;
	QTableWidget*	m_filesTable;

    Ui::MainWindow* m_ui;
};
////////////////////////////////////////////////////////////////////////////////

inline
QStringList	MainWindow::getFileNames()	const
{
	return	m_fileNames;
}
////////////////////////////////////////////////////////////////////////////////

#endif // MAINWINDOW_H
