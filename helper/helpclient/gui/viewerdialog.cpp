
/*****************************************************************************
FILE		: viewerdialog.cpp
DESCRIPTION	: Session viewer dialog.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "viewerdialog.h"
#include "ui_viewerdialog.h"

#include "../core.h"
#include "../session/session.h"
#include "../globaldefinitions.h"
#include "../session/sessioncommunicator.h"
#include "../session/statemachine/sessionthread.h"

#include <QMutex>
#include <QTimer>
#include <QMutexLocker>
#include <QApplication>
#include <QDesktopWidget>

////////////////////////////////////////////////////////////////////////////////

#define	SYNC	 QMutexLocker	locker( m_mutex );

////////////////////////////////////////////////////////////////////////////////

ViewerDialog::ViewerDialog( QWidget* parent ) : QFrame( parent ),
    m_ui( new Ui::ViewerDialog ),
    m_mutex( NULL ),
	m_updateRect( 0, 0, 0, 0 ),
    m_updateScreenTimer( NULL )
{
    m_ui->setupUi(this);

    m_mutex				= new QMutex();
    m_updateScreenTimer	= new QTimer();

	//connect( currentSession, SIGNAL(updatesReceived()), this, SLOT(onUpdatesReceived()), Qt::QueuedConnection );

	m_ui->lblScreenImage->hide();
	setWindowTitle( QString::fromUtf8("%1-%2").arg(APP_NAME).arg("Viewer screen") );
	setMouseTracking( true );
/*
    connect( m_updateScreenTimer, SIGNAL(timeout()), SLOT(onUpdateScreenTimeout()) );
    m_updateScreenTimer->start( 33 );*/
}
////////////////////////////////////////////////////////////////////////////////

ViewerDialog::~ViewerDialog()
{
    if( m_mutex != NULL )
    {
		delete	m_mutex;
		m_mutex	= NULL;
    }
    if( m_updateScreenTimer != NULL )
    {
		delete m_updateScreenTimer;
		m_updateScreenTimer = NULL;
    }

    delete m_ui;
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::createInitialScreen( int width, int height, QByteArray& imageData )
{
    SYNC;
	m_image	= QImage( QSize(width, height), QImage::Format_RGB32 );
	memcpy( m_image.bits(), (unsigned char*)imageData.data(), imageData.size() );
	
	/*m_image	= QImage( (unsigned char*)imageData.data(), width, height, QImage::Format_RGB32 );*/

	m_presenterImgSize	= QSize( width, height );
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::updateViewerScene( QPainter& painter, const QRect& updateRect )
{
    SYNC;
	if( ! m_scaledImg.isNull() )
		painter.drawImage( updateRect, m_scaledImg, updateRect );

   /* QPixmap pixmap  = QPixmap::fromImage( m_scaledImg );
    m_ui->lblScreenImage->resize( pixmap.size() );
    m_ui->lblScreenImage->setPixmap( pixmap );
    m_ui->lblScreenImage->move( QPoint(0, 0) );*/
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::onUpdateScreen()
{
    /*SessionThread* thread = g_Core->getSessionThread();

    if( thread != NULL )
    {
		Session*	session = thread->getSession();

		if( session != NULL )
		{
			updateViewerScene();
		}
    }*/

	if( m_updateRect.isValid() )
	{
		m_scaledImg	= m_image.scaled( geometry().size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );

		repaint( m_updateRect );

		m_updateRect	= QRect( 0, 0, 0, 0 );
	}
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::onFirstScreenReceived()
{
	//m_image.save( QString::fromUtf8("C:\\Users\\s_zhelev\\Desktop\\asd\\screen.jpg"), "JPG" );
	/*connect( m_updateScreenTimer, SIGNAL(timeout()), SLOT(onUpdateScreenTimeout()) );
    m_updateScreenTimer->start( 33 );*/
	
	show();

	repositionWindow();
	m_scaledImg	= m_image.scaled( geometry().size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );

	raise();
	activateWindow();
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::paintEvent( QPaintEvent* event )
{
	QPainter	painter( this );

	const QRect&	updateRect	= event->rect();

	updateViewerScene( painter, updateRect );
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::onUpdateScreenTimeout()
{
	bool	canUpdate	= false;
	
	Session*	currentSession	= g_Core->getSessionThread()->getSession();
	QByteArray*	updateData		= currentSession->popUpdate();

	QPainter	painter;
	painter.begin( &m_image );
	
	quint16	xPos		= -1;
	quint16	yPos		= -1;

	char*	updateX	= (char*)&xPos;
	char*	updateY	= (char*)&yPos;

	while( updateData != NULL )
	{
		updateX[0]		= updateData->at( 0 );
		updateX[1]		= updateData->at( 1 );
		
		updateY[0]		= updateData->at( 2 );
		updateY[1]		= updateData->at( 3 );

		updateData->remove( 0, 4 );

		int	updWidth	= BOX_WIDTH;
		int	updHeight	= BOX_HEIGHT;

		QImage	img( QSize(BOX_WIDTH, BOX_HEIGHT), QImage::Format_RGB32 );
			
		//	Copy uncompressed image data to image buffer.
		memcpy( img.bits(), (unsigned char*)updateData->data(), updateData->size() );

		painter.drawImage( QPoint(xPos, yPos), img );

		int	updX		= (int)xPos;
		int	updY		= (int)yPos;
		

		posToDrawCoords( updX, updY );
		sizeToDrawDimension( updWidth, updHeight );
		
		//qDebug(  "RECEIVING UPDATE( %d, %d) > ( %d, %d ) ", updX, updY, updWidth, updHeight );
		m_updateRect	= m_updateRect.united( QRect(updX, updY, updWidth, updHeight) );
		
		currentSession->popOldUpdate( 0 );
		
		canUpdate	= true;
		updateData	= currentSession->popUpdate();
	}

	if( canUpdate )
	{
		painter.end();
		onUpdateScreen();
	}
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::keyPressEvent( QKeyEvent*	event )
{
	SessionCommunicator* communicator = g_Core->getSessionThread()->getCommunicator();

    if( communicator != NULL )
    {
		QByteArray	data;
		quint16	nativeKey	= (quint16)event->nativeVirtualKey();
			
		data[0]	= (quint16)PACKET_SIMULATE_EVENT;
		data[1]	= (quint16)PACKET_SIMULATE_EVENT	>> 8;

		data[2]	= (quint16)4;
		data[3]	= (quint16)4	>> 8;

		data[4]	= (quint16)KEY_PRESSED;
		data[5]	= (quint16)KEY_PRESSED	>> 8;
			
		data[6]	= nativeKey;
		data[7]	= nativeKey	>> 8;

		communicator->pushData( data );
		/*
		
		

		*/
    }
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::keyReleaseEvent(  QKeyEvent* event )
{
	SessionCommunicator* communicator = g_Core->getSessionThread()->getCommunicator();

    if( communicator != NULL )
    {
		QByteArray	data;
		quint16	nativeKey	= (quint16)event->nativeVirtualKey();
			
		data[0]	= (quint16)PACKET_SIMULATE_EVENT;
		data[1]	= (quint16)PACKET_SIMULATE_EVENT	>> 8;

		data[2]	= (quint16)4;
		data[3]	= (quint16)4	>> 8;

		data[4]	= (quint16)KEY_RELEASED;
		data[5]	= (quint16)KEY_RELEASED	>> 8;
			
		data[6]	= nativeKey;
		data[7]	= nativeKey	>> 8;
		
		/*QByteArray	getUpdatesCmd;
	
		getUpdatesCmd[0]	= (quint16)PACKET_GET_UPDATES;
		getUpdatesCmd[1]	= (quint16)PACKET_GET_UPDATES	>> 8;
	
		getUpdatesCmd[2]	= (quint16)0;
		getUpdatesCmd[3]	= (quint16)0	>> 8;*/
		
		communicator->pushData( data );
		//communicator->pushData( getUpdatesCmd );
    }
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::mousePressEvent( QMouseEvent* event )
{
	QByteArray	data;
	data[0]	= (quint16)PACKET_SIMULATE_EVENT;
	data[1]	= (quint16)PACKET_SIMULATE_EVENT	>> 8;

	data[2]	= (quint16)2;
	data[3]	= (quint16)2	>> 8;

	if( event->button() == Qt::LeftButton )
	{
		data[4]	= (quint16)MOUSE_LEFT_PRESSED;
		data[5]	= (quint16)MOUSE_LEFT_PRESSED	>> 8;
	}
	else
	{
		data[4]	= (quint16)MOUSE_RIGHT_PRESSED;
		data[5]	= (quint16)MOUSE_RIGHT_PRESSED	>> 8;
	}

	g_Core->getSessionThread()->getCommunicator()->pushData( data );
		emit	sendEvents();
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::mouseReleaseEvent( QMouseEvent* event )
{
	QByteArray	data;
	data[0]	= (quint16)PACKET_SIMULATE_EVENT;
	data[1]	= (quint16)PACKET_SIMULATE_EVENT	>> 8;

	data[2]	= (quint16)2;
	data[3]	= (quint16)2	>> 8;

	if( event->button() == Qt::LeftButton )
	{
		data[4]	= (quint16)MOUSE_LEFT_RELEASED;
		data[5]	= (quint16)MOUSE_LEFT_RELEASED	>> 8;
	}
	else
	{
		data[4]	= (quint16)MOUSE_RIGHT_RELEASED;
		data[5]	= (quint16)MOUSE_RIGHT_RELEASED	>> 8;
	}

	g_Core->getSessionThread()->getCommunicator()->pushData( data );
		emit	sendEvents();
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::mouseMoveEvent( QMouseEvent* event )
{
	QByteArray	data;
	data[0]	= (quint16)PACKET_SIMULATE_EVENT;
	data[1]	= (quint16)PACKET_SIMULATE_EVENT	>> 8;

	data[2]	= (quint16)6;
	data[3]	= (quint16)6	>> 8;

	QPoint	realPoint	= toRealScreenCoords( event->pos() );
	
	int		x	= realPoint.x();
	int		y	= realPoint.y();
	
	data[4]	= (quint16)MOUSE_MOVE;
	data[5]	= (quint16)MOUSE_MOVE	>> 8;

	data[6]	= (quint16)x;
	data[7]	= (quint16)x	>> 8;

	data[8]	= (quint16)y;
	data[9]	= (quint16)y	>> 8;

	g_Core->getSessionThread()->getCommunicator()->pushData( data );
		emit	sendEvents();
}
////////////////////////////////////////////////////////////////////////////////

QPoint	ViewerDialog::toRealScreenCoords( const QPoint& point )
{
	const QRect&	rect	= geometry();

	int	drawWidth	= rect.width();
	int	drawHeight	= rect.height();

	double	kx	= (double)drawWidth  / (double)m_presenterImgSize.width();
	double	ky	= (double)drawHeight / (double)m_presenterImgSize.height();

	if( kx < ky )
		ky	= kx;
	else
		kx	= ky;
	
	//	Scale the coordinates from draw to screen
	QPoint	realPos;
	realPos.setX( (int)((double)point.x() / kx) );
	realPos.setY( (int)((double)point.y() / ky) );

	return	realPos;
}
/////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::repositionWindow()
{
	if( isMaximized() || isFullScreen() )
		showNormal();

	int				width			= m_presenterImgSize.width();
	int				height			= m_presenterImgSize.height();
	int				activeScreen	= QApplication::desktop()->primaryScreen();
	QRect			desktopRect		= QApplication::desktop()->availableGeometry( activeScreen );
	QRect			wndRect			= frameGeometry();
	const QRect&	clRect			= geometry();

	int	junkX	= wndRect.width()  - clRect.width();
	int	junkY	= wndRect.height() - clRect.height();

	//	Calc the window size if the client area have dimensions of the image.
	int	newWndWidth		= width + junkX;
	int	newWndHeight	= height + junkY;

	//	If the viewer dialog can visualize the whole presenter screen.
	if( newWndWidth <= desktopRect.width() && newWndHeight <= desktopRect.height() )
	{
		//	The viewer dialog can be enlarged to show the presenter desktop without stretching it.
		int	x	= ( desktopRect.width()  - newWndWidth ) / 2;
		int	y	= ( desktopRect.height() - newWndHeight ) / 2;

		//	In case we have dock menu on MacOS
		if( y < desktopRect.y() )
			y = desktopRect.y();

		move( x, y );
		resize( width, height );
	}
	else
	{
		//	The screen dimension is not enough to show the image in 1:1 and we have to fit it to window

		//	New Client Area dimensions
		int	ncw	= 0;
		int	nch	= 0;
		int	x	= 0;
		int	y	= 0;
	
		//	Desktop dimensions
		int	dw	= desktopRect.width();
		int	dh	= desktopRect.height();

		//	Perform a BEST-FIT algorithm
		double	ratioDesktop	= (double)dw    / (double)dh;		//	Desktop aspect ratio
		double	ratioBitmap		= (double)width / (double)height;	//	Image  aspect ratio

		if( ratioBitmap > ratioDesktop )
		{
			//	+----------+
			//	+----------+
			//	|          |
			//	+----------+
			//	+----------+
			
			//	ncw	remains the same as screen width
			ncw	= dw - junkX;
			nch	= (int)( (((double)(dw-junkX) * (double)height) / (double)width) );

			x	= 0;
			y	= (dh - nch) / 2;	//	Center vertically (desktop height - client height) / 2
		}
		else
		{
			//	+--+----+--+
			//	|  |    |  |
			//	|  |    |  |
			//	|  |    |  |
			//	+--+----+--+

			//	ncw	remains the same as screen width
			nch	= desktopRect.height() - junkY;
			ncw	= (int)( (((double)(dh-junkY) * (double)width) / (double)height) );

			x	= (dw - ncw) / 2;	//	Center vertically (desktop width - client width) / 2
			y	= 0;
		}

		//	Multiple monitors support: Offset the position regarding the selected monitor
		x	= desktopRect.left() + x;
		y	= desktopRect.top()  + y;

		move( x, y );
		resize( ncw, nch );
	}
}
/////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::posToDrawCoords( int& x, int& y )	const
{
	const QRect&	rect	= this->rect();

	int	wndWidth	= rect.width();
	int	wndHeight	= rect.height();

	int	presenterScrWidth	= m_presenterImgSize.width();
	int	presenterScrHeight	= m_presenterImgSize.height();

	double	kx	= double(wndWidth) / double(presenterScrWidth);
	double	ky	= double(wndHeight) / double(presenterScrHeight);

	if( kx < ky )
		ky	= kx;
	else
		kx	= ky;

	x	= rect.left() + (int)((double(x) * kx) + 0.5);
	y	= rect.top()  + (int)((double(y) * ky) + 0.5);
}
/////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::sizeToDrawDimension( int& width, int& height )	const
{
	const QRect&	rect	= this->rect();

	int	wndWidth	= rect.width();
	int	wndHeight	= rect.height();

	int	presenterScrWidth	= m_presenterImgSize.width();
	int	presenterScrHeight	= m_presenterImgSize.height();

	double	kx	= double(wndWidth) / double(presenterScrWidth);
	double	ky	= double(wndHeight) / double(presenterScrHeight);

	if( kx < ky )
		ky	= kx;
	else
		kx	= ky;

	width	= (int)((double(width)  * kx) + 0.5);
	height	= (int)((double(height) * ky) + 0.5);
}
/////////////////////////////////////////////////////////////////////////////
