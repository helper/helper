
#ifndef __VIEWERDIALOG_H__
#define __VIEWERDIALOG_H__

/*****************************************************************************
FILE		: viewerdialog.h
DESCRIPTION	: Session viewer dialog.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include <QImage>
#include <QFrame>
#include <QPainter>
#include <QKeyEvent>

////////////////////////////////////////////////////////////////////////////////

class	QMutex;
class	QTimer;
class	QMutexLocker;

////////////////////////////////////////////////////////////////////////////////

namespace Ui
{
    class ViewerDialog;
}
////////////////////////////////////////////////////////////////////////////////

class ViewerDialog : public QFrame
{
    Q_OBJECT
    
public:
    explicit ViewerDialog( QWidget* parent = 0 );
    ~ViewerDialog();

	void	createInitialScreen( int width, int height, QByteArray& imageData );

private slots:

	void	onUpdateScreenTimeout();
	void	onFirstScreenReceived();

signals:

	void	sendEvents();

protected:
	virtual	void	paintEvent( QPaintEvent* event );
	virtual	void	keyPressEvent(  QKeyEvent* event );
	virtual	void	keyReleaseEvent(  QKeyEvent* event );
	virtual	void	mousePressEvent(  QMouseEvent* event );
	virtual	void	mouseReleaseEvent(  QMouseEvent* event );
	virtual	void	mouseMoveEvent(  QMouseEvent* event );

private:
	
	QPoint	toRealScreenCoords( const QPoint& point );
	void	repositionWindow();
	void	posToDrawCoords( int& x, int& y )	const;
	void	sizeToDrawDimension( int& width, int& height )	const;

    void    updateViewerScene( QPainter& painter, const QRect& updateRect );
	
    void    onUpdateScreen();

    Ui::ViewerDialog*	m_ui;
	
	QRect	m_updateRect;
	QImage	m_image;
	QImage	m_scaledImg;
    QMutex*	m_mutex;
    QPixmap	m_screen;
    QTimer*	m_updateScreenTimer;

	QSize	m_presenterImgSize;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __VIEWERDIALOG_H__
