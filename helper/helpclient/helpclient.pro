# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

TEMPLATE = app
TARGET = helpclient
DESTDIR = ./debug
QT += core gui network qaxserver qaxcontainer
CONFIG += debug
DEFINES += _WINDOWS QT_LARGEFILE_SUPPORT QT_NETWORK_LIB QT_HAVE_MMX QT_HAVE_3DNOW QT_HAVE_SSE QT_HAVE_MMXEXT QT_HAVE_SSE2
INCLUDEPATH += ./debug \
    . \
    $(QTDIR)/mkspecs/win32-msvc2010 \
    ./ui \
    ./qtgenerated \
    ./session \
    ./filetransfer
LIBS += -l../utilities/debug/utilities \
    -lgdi32 \
    -lcomdlg32 \
    -loleaut32 \
    -limm32 \
    -lwinmm \
    -lwinspool \
    -lws2_32 \
    -lole32 \
    -luser32 \
    -ladvapi32 \
    -lmsimg32 \
    -lshell32 \
    -lkernel32 \
    -luuid
PRECOMPILED_HEADER = StdAfx.h
DEPENDPATH += .
MOC_DIR += debug
OBJECTS_DIR += debug
UI_DIR += ./qtgenerated
RCC_DIR += ./qtgenerated
include(helpclient.pri)
