
/*****************************************************************************
FILE		: screensessionmanager.cpp
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "screensessionmanager.h"

#include "session/statemachine/SessionThread.h"

////////////////////////////////////////////////////////////////////////////////

#define	SYNC	 QMutexLocker	locker( &m_mutex )

////////////////////////////////////////////////////////////////////////////////

ScreenSessionManager::ScreenSessionManager() : m_sessions( NULL )
{

}
////////////////////////////////////////////////////////////////////////////////

SessionThread* ScreenSessionManager::getSessionThread( int idx )   const
{
    SYNC;
    return  m_sessions.at( idx );
}
////////////////////////////////////////////////////////////////////////////////

void	ScreenSessionManager::startNewSession( bool isHost )
{
    SessionThread* SessionThread  = new SessionThread( isHost );
    SessionThread->setID( m_sessions.size() );
    SessionThread->start();

    SYNC;
    m_sessions.append( SessionThread );
}
////////////////////////////////////////////////////////////////////////////////

void	ScreenSessionManager::endSession( int sessionID )
{
    SYNC;
    m_sessions.removeAt( sessionID );
}
////////////////////////////////////////////////////////////////////////////////
