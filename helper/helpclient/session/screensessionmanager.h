
/*****************************************************************************
FILE		: screensessionmanager.h
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef __SCREENSESSIONMANAGER_H__
#define __SCREENSESSIONMANAGER_H__

////////////////////////////////////////////////////////////////////////////////

#include <QList>
#include <QMutex>
#include <QImage>
#include <QObject>

////////////////////////////////////////////////////////////////////////////////

class	QMutexLocker;
class	SessionThread;

////////////////////////////////////////////////////////////////////////////////

class ScreenSessionManager
{
public:

    ScreenSessionManager();

    SessionThread*	getSessionThread( int idx ) const;

    void    startNewSession( bool isHost );
    void    endSession( int sessionID );

private:

    QMutex		    m_mutex;
    QList<SessionThread*>  m_sessions;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __SCREENSESSIONMANAGER_H__
