
/*****************************************************************************
FILE		: Session.cpp
DESCRIPTION	: Session thread class

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include "session.h"

#include <QImage>
#include <QMutex>
#include <QTimer>
#include <QBuffer>
#include <QPainter>
#include <QTcpServer>
#include <QMutexLocker>
#include <QApplication>
#include <QNetworkInterface>

#include "../core.h"
#include "../capture/capturethread.h"
#include "../gui/viewerdialog.h"

#include "sessioncommunicator.h"
#include "../globaldefinitions.h"
#include "statemachine/sessionthread.h"

////////////////////////////////////////////////////////////////////////////////

#define	SYNC	 QMutexLocker	locker( &m_mutex )

////////////////////////////////////////////////////////////////////////////////

Session::Session( bool presenter ) : QObject(),
	m_screenBytes( -1 ), 
	m_screenWidth( -1 ), 
	m_screenHeight( -1 ),
    m_isPresenter( presenter ),
	m_firstScreenSend( false ),
	m_firstScreenReceived( false ),
    m_server( NULL ),
    m_socket( NULL ),
	m_isConnected( false ),
	m_dataAvailable( false ),
	m_hasNewConnection( false ),
	m_image( NULL ),
	m_oldImage( NULL )
{
	if( ! m_isPresenter )
	{
		ViewerDialog*	viewerDlg	= g_Core->getViewerDlg();
		connect( this, SIGNAL(firstScreenReceived()), viewerDlg, SLOT(onFirstScreenReceived()), Qt::QueuedConnection );
		connect( this, SIGNAL(updatesAvailable()), viewerDlg, SLOT(onUpdateScreenTimeout()), Qt::QueuedConnection );
	}
}
////////////////////////////////////////////////////////////////////////////////

Session::~Session()
{
	if( m_image != NULL )
	{
		delete	m_image;
		m_image	= NULL;
	}

    if( m_server != NULL )
	{
		delete	m_server;
		m_server    = NULL;
    }

    if( m_socket != NULL )
    {
		delete m_socket;
		m_socket    = NULL;
    }
}
////////////////////////////////////////////////////////////////////////////////

void	Session::pushUpdates( const QList<QByteArray>& updates )
{
	SYNC;
	m_updates	= updates;
}
////////////////////////////////////////////////////////////////////////////////

void	Session::setScreen( const QImage& img )
{
	SYNC;
	memcpy( m_image->bits(), img.bits(), img.width()*img.height()*4 );
    //m_image = img;
}
////////////////////////////////////////////////////////////////////////////////

QImage	Session::screen()
{
    SYNC;
    return  *m_image;
}
////////////////////////////////////////////////////////////////////////////////

void	Session::sendUpdates()
{
	int	counter			= 0;

	while( m_updates.size() > 0 )
	{
		m_socket->write( m_updates[0] );
		m_updates.removeAt( 0 );

		counter++;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onSendFullScreen()
{
	g_Core->captureDesktop();
	g_Core->getSessionThread()->getCommunicator()->send();

	m_firstScreenSend=true;

	/*QTimer*	updatesTimer	= new QTimer( this );
	connect( updatesTimer, SIGNAL(timeout()), SLOT(onSendUpdates()) );

	updatesTimer->start( 100 );*/
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onSendUpdates()
{
	//g_Core->captureDesktop();

	SYNC;
	int	updatesCount	= m_updates.size();
	int	idx		= 0;
	
	SessionCommunicator*	communicator	= g_Core->getSessionThread()->getCommunicator();

	while( idx < updatesCount )
	{
		communicator->pushData( m_updates[idx] );
		idx++;
	}

	communicator->send();
	m_updates.clear();
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onEventInfo( const QByteArray&	eventInfo )
{
	QByteArray	info	= eventInfo;
	onSimulateEvent( info );
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onScreenChunk( const QByteArray& screenInfo )
{
	m_screenArray.append( screenInfo );

	if( ! m_firstScreenReceived )
	{
		qDebug( "TOTAL SCREEN SIZE(%dx%d) IN COMPRESSED BYTES > %d", m_screenWidth, m_screenHeight, m_screenBytes );

		qDebug() << m_screenArray.size();
		
		if( m_screenArray.size() >= m_screenBytes  )
		{
			m_firstScreenReceived	= true;
		
			QByteArray	uncompressed	= qUncompress( m_screenArray );

			ViewerDialog*	vDialog	= g_Core->getViewerDlg();
			vDialog->createInitialScreen( m_screenWidth, m_screenHeight, uncompressed );

			//	Clear buffer and reset bytes count.
			m_screenBytes = -1;
			m_screenArray.clear();
			
			/*SessionCommunicator*	communicator	= g_Core->getSessionThread()->getCommunicator();

			QByteArray	getUpdatesCmd;
	
			getUpdatesCmd[0]	= (quint16)PACKET_GET_UPDATES;
			getUpdatesCmd[1]	= (quint16)PACKET_GET_UPDATES	>> 8;
	
			getUpdatesCmd[2]	= (quint16)0;
			getUpdatesCmd[3]	= (quint16)0	>> 8;

			communicator->pushData( getUpdatesCmd );*/
			//communicator->sendData();

			emit firstScreenReceived();
		}
	}
	else
	{
		if( m_screenArray.size() > 8 )
		{
			quint16	packetType	= -1;
			quint16	xPos		= -1;
			quint16	yPos		= -1;
			quint16	numBytes	= -1;

			char*	packet	= (char*)&packetType;
			char*	updateX	= (char*)&xPos;
			char*	updateY	= (char*)&yPos;
			char*	bytes	= (char*)&numBytes;
		
			packet[0]		= m_screenArray[0];
			packet[1]		= m_screenArray[1];

			updateX[0]		= m_screenArray[2];
			updateX[1]		= m_screenArray[3];

			updateY[0]		= m_screenArray[4];
			updateY[1]		= m_screenArray[5];
		
			bytes[0]		= m_screenArray[6];
			bytes[1]		= m_screenArray[7];

			if( m_screenArray.size() >= numBytes + 8 )
			{
				m_screenArray.remove( 0, 8 );
			
				QByteArray	data;
				data.resize( numBytes );

				memcpy( data.data(), m_screenArray.data(), numBytes );

				data.prepend( updateY, 2 );
				data.prepend( updateX, 2 );
				
				SYNC;
				m_updates.append( data );
				m_screenArray.remove( 0, numBytes );

				qDebug( "PROCESSING ( %d, %d ) > %d , %d, %d", xPos, yPos, numBytes, packetType, m_updates.size() );

				if( packetType == LAST_PACKET )
				{
					qDebug() << "LAST PACKET RECEIVED";
					
					/*SessionCommunicator*	communicator	= g_Core->getSessionThread()->getCommunicator();

					QByteArray	getUpdatesCmd;

					getUpdatesCmd[0]	= (quint16)PACKET_GET_UPDATES;
					getUpdatesCmd[1]	= (quint16)PACKET_GET_UPDATES	>> 8;
	
					getUpdatesCmd[2]	= (quint16)0;
					getUpdatesCmd[3]	= (quint16)0	>> 8;

					communicator->pushData( getUpdatesCmd );
					communicator->sendData();*/
					
					//	Notify GUI to update presenter screen.
					emit updatesAvailable();
					//	Return command "GET UPDATES" to host.
					//stream << COMMAND_GET_UPDATES;
				}
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onStreaming()
{
	qDebug() << "I AM STREAMING UPDATES NOW!";
//	onSendUpdates();
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onUpdates()
{
	m_screenArray.append( m_socket->readAll() );

	if( m_screenArray.size() > 8 )
	{
		m_firstScreenReceived	= true;

		quint16	packetType	= -1;
		quint16	xPos		= -1;
		quint16	yPos		= -1;
		quint16	numBytes	= -1;

		char*	packet	= (char*)&packetType;
		char*	updateX	= (char*)&xPos;
		char*	updateY	= (char*)&yPos;
		char*	bytes	= (char*)&numBytes;
		
		packet[0]		= m_screenArray[0];
		packet[1]		= m_screenArray[1];

		updateX[0]		= m_screenArray[2];
		updateX[1]		= m_screenArray[3];

		updateY[0]		= m_screenArray[4];
		updateY[1]		= m_screenArray[5];
		
		bytes[0]		= m_screenArray[6];
		bytes[1]		= m_screenArray[7];

		if( m_screenArray.size() >= numBytes + 8 )
		{
			m_screenArray.remove( 0, 8 );
			
			QByteArray	data;
			data.resize( numBytes );

			memcpy( data.data(), m_screenArray.data(), numBytes );

			//if( data.size() > 0 )
			{
				//QImage	img( QSize(BOX_WIDTH, BOX_HEIGHT), QImage::Format_RGB32 );
			
				//	Copy uncompressed image data to image buffer.
				//memcpy( img.bits(), (unsigned char*)data.data(), data.size() );

				//QPainter	painter( &m_image );
				//painter.drawImage( QPoint(xPos, yPos), img );
				//img.save( QString::fromUtf8("C:\\Users\\SG\\Desktop\\updates\\update_%1_%2.jpg").arg( m_updateX ).arg( m_updateY ), "JPG" );
				
				data.prepend( updateY, 2 );
				data.prepend( updateX, 2 );
				
				SYNC;
				m_updates.append( data );

				m_screenArray.remove( 0, numBytes );
			}
			
			if( packetType == LAST_PACKET )
			{
				qDebug() << "LAST PACKET RECEIVED";

				QDataStream	stream( m_socket );
				stream.setVersion( QDataStream::Qt_4_0 );
			
				//	Return command "GET UPDATES" to host.
				//stream << COMMAND_GET_UPDATES;
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onSimulateEvent( QByteArray& data )
{
	quint16	evtType	= -1;

	char*	eventType	= (char*)&evtType;

	eventType[0]	= data[0];
	eventType[1]	= data[1];

	switch( evtType )
	{
	case	KEY_PRESSED:
		{
			quint16	keyEventVKey	= -1;

			char*	eventVKey	= (char*)&keyEventVKey;
	eventVKey[0]	= data[2];
	eventVKey[1]	= data[3];

		m_rcPlayer.playKeyPressed( (int)keyEventVKey );
		}
		break;
	case	KEY_RELEASED:
		{
			quint16	keyEventVKey	= -1;
			char*	eventVKey		= (char*)&keyEventVKey;

			eventVKey[0]	= data[2];
			eventVKey[1]	= data[3];

		m_rcPlayer.playKeyReleased( (int)keyEventVKey );
		}
		break;
	case	MOUSE_LEFT_PRESSED:
		m_rcPlayer.playMousePressed( true );
		break;
	case	MOUSE_LEFT_RELEASED:
		m_rcPlayer.playMouseReleased( true );
		break;
	case	MOUSE_RIGHT_PRESSED:
		m_rcPlayer.playMousePressed( false );
		break;
	case	MOUSE_RIGHT_RELEASED:
		m_rcPlayer.playMouseReleased( false );
		break;
	case	MOUSE_MOVE:
		{
			quint16	x		= -1;
			char*	xPos	= (char*)&x;

			xPos[0]	= data[2];
			xPos[1]	= data[3];
			
			quint16	y		= -1;
			char*	yPos	= (char*)&y;

			yPos[0]	= data[4];
			yPos[1]	= data[5];

			m_rcPlayer.playMouseMove( x, y );
		}
		break;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	Session::appendKeyEvent( const QByteArray keyEvent )
{
	SYNC;
	m_keyEvents.append( keyEvent );
}
////////////////////////////////////////////////////////////////////////////////

void	Session::sendKeyEvents()
{
	QDataStream	stream( m_socket );
	stream.setVersion( QDataStream::Qt_4_0 );
			
	while( m_keyEvents.size() > 0 )
	{
		//	Return command "SIMULATE KEY EVENT" to host.
		//stream << COMMAND_SIMULATE_KEY_EVENT;

		m_socket->write( m_keyEvents.at(0) );
		m_keyEvents.removeAt( 0 );
	}
	
	//stream << COMMAND_GET_UPDATES;
}
////////////////////////////////////////////////////////////////////////////////

void	Session::popOldUpdate( int index )
{
	SYNC;
	m_updates.removeAt( index );
}
////////////////////////////////////////////////////////////////////////////////

QByteArray*	Session::popUpdate()
{
	SYNC;
	if( m_updates.size() <= 0 )
		return NULL;

	return	&m_updates[0];
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onConnectionEstablished()
{
	if( m_isPresenter && ! m_firstScreenSend )
		onSendFullScreen();
}
//////////////////////////////////////////////////////////////////////////////////
