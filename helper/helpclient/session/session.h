
#ifndef __SESSION_H__
#define __SESSION_H__

/*****************************************************************************
FILE		: Session.h
DESCRIPTION	: Session thread class

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include <QMutex>
#include <QImage>
#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>

#include "../../utilities/rcplayer/remotecontrolplayer.h"

////////////////////////////////////////////////////////////////////////////////

class	QTcpServer;
class	QMutexLocker;
class	SessionThread;

////////////////////////////////////////////////////////////////////////////////

class Session : public QObject
{
	Q_OBJECT
		
public:
    Session( bool presenter );
    ~Session();

    void    setPresenter( bool present );
    void    setScreen( const QImage& img );
	
	QHostAddress	getHostAddress()	const;
    void			setHostAddress( const QHostAddress& m_hostAddress );

    void    setScreenSize( const QSize&	size );
	QSize	getScreenSize()	const;

    void    initializeSocketAndServer();
	void	pushUpdates( const QList<QByteArray>& updates );

    bool    isPresenter()	const;
	bool	hasUpdates()	const;
	bool	hasKeyEvents()	const;
	void	onNewScreenInfo( int width, int height, int sizeInBytes );
	void	onEventInfo( const QByteArray&	eventInfo );
	void	onScreenChunk( const QByteArray& screenInfo );
	void	onStreaming();

    QImage  screen();

   /* void    onListen();
    void    onConnect();
    void    onReadyRead();
    void    onConnected();
    void    onInitialize();
    void    onNewConnection();*/

    /*bool    onWaitForRead();
    bool    onWaitForConnected();
    bool    onWaitForConnection();*/

	void	sendUpdates();
	void	appendKeyEvent(	const QByteArray keyEvent );
	void	sendKeyEvents();
	
	void	popOldUpdate( int index );
	QByteArray*	popUpdate();
	
signals:
	void	firstScreenReceived();
	void	updatesAvailable();

public	slots:
	/*void	onHostFound();
	void	onError( QAbstractSocket::SocketError sockError );
	void	onDataAvailable();
	void	onUserConnection();*/
	void	onSendUpdates();
	void	onConnectionEstablished();

private:

	int		m_screenWidth;
	int		m_screenHeight;
    int	    m_screenBytes;

    bool    m_isPresenter;
	bool	m_firstScreenSend;
	bool	m_firstScreenReceived;
	
	bool	m_isConnected;
	bool	m_dataAvailable;
	bool	m_hasNewConnection;

    QMutex			m_mutex;
    QImage*			m_image;
	
    QTcpSocket*	    m_socket;
    QTcpServer*	    m_server;

	QImage*			m_oldImage;
    QByteArray		m_screenArray;
	QHostAddress	m_hostAddress;

	QList<QByteArray>	m_updates;
	QList<QByteArray>	m_keyEvents;


	RemoteControlPlayer	m_rcPlayer;
	
	void	onSendFullScreen();
	
	void	onUpdates();

	void	onSimulateEvent( QByteArray& data );
};
////////////////////////////////////////////////////////////////////////////////

inline
bool	Session::isPresenter()  const
{
    return  m_isPresenter;
}
////////////////////////////////////////////////////////////////////////////////

inline
bool	Session::hasUpdates()	const
{
	return	m_updates.size() > 0;
}
////////////////////////////////////////////////////////////////////////////////

inline
bool	Session::hasKeyEvents()	const
{
	return	m_keyEvents.size()	> 0;
}
////////////////////////////////////////////////////////////////////////////////

inline
void	Session::onNewScreenInfo( int width, int height, int sizeInBytes )
{
	m_screenWidth	= width;
	m_screenHeight	= height;
	m_screenBytes	= sizeInBytes;
}
////////////////////////////////////////////////////////////////////////////////

inline
void	Session::setPresenter( bool present )
{
    m_isPresenter   = present;
}
////////////////////////////////////////////////////////////////////////////////

inline
QHostAddress	Session::getHostAddress()	const
{
	return	m_hostAddress;
}
////////////////////////////////////////////////////////////////////////////////

inline
void	Session::setHostAddress( const QHostAddress& hostAddress )
{
	m_hostAddress	= hostAddress;
}
////////////////////////////////////////////////////////////////////////////////

inline
void	Session::setScreenSize( const QSize& scrSize )
{
	m_screenWidth	= scrSize.width();
	m_screenHeight	= scrSize.height();

	m_image		= new QImage( m_screenWidth, m_screenHeight, QImage::Format_RGB32 );
	m_oldImage	= new QImage( m_screenWidth, m_screenHeight, QImage::Format_RGB32 );
}
////////////////////////////////////////////////////////////////////////////////

inline
QSize	Session::getScreenSize()	const
{
	return	QSize( m_screenWidth, m_screenHeight );
}
////////////////////////////////////////////////////////////////////////////////

#endif // __SESSION_H__
