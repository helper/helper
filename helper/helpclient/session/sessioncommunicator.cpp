#include "sessioncommunicator.h"

#include "session.h"
#include "../core.h"
#include "../gui/viewerdialog.h"
#include "../globaldefinitions.h"
//#include "statemachine/statesend.h"
#include "statemachine/sessionthread.h"
#include "statemachine/stateinitialize.h"
//#include "statemachine/statestreaming.h"

#include <QBuffer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QHostAddress>
#include <QNetworkInterface>

#include <assert.h>

////////////////////////////////////////////////////////////////////////////////

SessionCommunicator::SessionCommunicator() : QObject(),
	m_server( NULL ),
	m_socket( NULL )
{
	bool	isViewer	= ! g_Core->getSessionThread()->getSession()->isPresenter();
	if( isViewer )
	{
		ViewerDialog*	viewerDlg	= g_Core->getViewerDlg();
		connect( viewerDlg, SIGNAL(sendEvents()), this, SLOT(onSendEvents()), Qt::QueuedConnection );
	}
}
////////////////////////////////////////////////////////////////////////////////

SessionCommunicator::~SessionCommunicator()
{
	if( m_server != NULL )
	{
		delete	m_server;
		m_server = NULL;
	}

	if( m_socket != NULL )
	{
		delete	m_socket;
		m_socket = NULL;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::start()
{
	changeState( StateInitialize::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onStart()
{
	bool	host	= g_Core->getSessionThread()->getSession()->isPresenter();

	if( host )
		startListen();
	else
		connectToHost( g_Core->getSessionThread()->getSession()->getHostAddress(), TCP_LOCAL_PORT );
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::startListen()
{
	m_server	= new QTcpServer();

	QHostAddress address;

	foreach( QNetworkInterface interf, QNetworkInterface::allInterfaces() )
		if( interf.flags().testFlag(QNetworkInterface::IsUp) && ( ! interf.flags().testFlag(QNetworkInterface::IsLoopBack)) )
			foreach( QNetworkAddressEntry entry, interf.addressEntries() )
				if( entry.ip().protocol() == QAbstractSocket::IPv4Protocol )
					address	= QHostAddress( entry.ip().toString() );

	m_server->listen( address, TCP_LOCAL_PORT );

	if( m_server->isListening() )
		qDebug() << "Started listening on : " << m_server->serverAddress() << ":" << m_server->serverPort();
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onScreenInfo( const QByteArray& screenInfo )
{
	quint16	width		= -1;
	quint16	height		= -1;
	int		sizeInBytes	= -1;

	char*	scrWidth	= (char*)&width;
	char*	scrHeight	= (char*)&height;
	char*	size		= (char*)&sizeInBytes;
		
	scrWidth[0]	= screenInfo[0];
	scrWidth[1]	= screenInfo[1];

	scrHeight[0]= screenInfo[2];
	scrHeight[1]= screenInfo[3];

	size[0]		= screenInfo[4];
	size[1]		= screenInfo[5];
	size[2]		= screenInfo[6];
	size[3]		= screenInfo[7];

	qDebug( "SCREEN INFO ( %d, %d ) > %d", width, height, sizeInBytes );

	Session*	session	= g_Core->getSessionThread()->getSession();
	session->onNewScreenInfo( width, height, sizeInBytes );
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onEventInfo(  const QByteArray& eventInfo )
{
	Session*	session	= g_Core->getSessionThread()->getSession();
	session->onEventInfo( eventInfo );
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::connectToHost( const QHostAddress& address, quint16 port )
{
	m_socket	= new QTcpSocket();

	/*connect( m_socket, SIGNAL(connected()),		this, SLOT(onConnected()) );
	connect( m_socket, SIGNAL(disconnected()),	this, SLOT(onDisconnected()) );
	connect( m_socket, SIGNAL(readyRead()),		this, SLOT(onReadyRead()) );
	connect( m_socket, SIGNAL(error(QAbstractSocket::SocketError)),	SLOT(onError(QAbstractSocket::SocketError)) );*/

	m_peerAddress	= address;

	m_socket->connectToHost( address, port, QIODevice::ReadWrite );
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onScreenChunk( const QByteArray& screenData )
{
	Session*	session	= g_Core->getSessionThread()->getSession();
	session->onScreenChunk( screenData );
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onGetUpdates()
{
	Session*	session	= g_Core->getSessionThread()->getSession();
	session->onStreaming();
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::sendData()
{
	//changeState( StateSend::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::send()
{
	int	datas	= m_data.size();
	int	idx		= 0;

	while( idx < datas )
	{
		m_socket->write( m_data[idx] );
		idx++;
	}
	m_data.clear();
	
	qDebug() << "DATA SEND IS : " << idx;
	//changeState( StateStreaming::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::notifyGuiConnected()
{
	emit	enableTabs();
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onNewConnection()
{
	m_socket		= m_server->nextPendingConnection();
	m_peerAddress	= m_socket->peerAddress();

	qDebug() << "New peer trying to join the desktop session : ";
    qDebug() << "Peer IP : " << m_peerAddress;
    qDebug() << "Peer port : " << m_socket->peerPort();

	g_Core->getSessionThread()->getSession()->onConnectionEstablished();
	//emit	connectionEstablished();
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onConnected()
{
	qDebug() << "CONNECTED > " << m_socket->peerAddress();
	
	emit	connectionEstablished();
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onDisconnected()
{
	qDebug() << "DISCONNECTED > " << m_socket->peerAddress();
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onReadyRead()
{
	/*qDebug() << "READY READ";*/

	if( m_socket->bytesAvailable() > 0 )
		m_inputData.append( m_socket->readAll() );
	
	int	totalData	= m_inputData.size();

	if(  totalData >= 4 )
	{
		quint16	packetType	= -1;
		quint16	dataSize	= -1;

		char*	packet		= (char*)&packetType;
		char*	inpDataSize	= (char*)&dataSize;

		packet[0]		= m_inputData[0];
		packet[1]		= m_inputData[1];

		inpDataSize[0]	= m_inputData[2];
		inpDataSize[1]	= m_inputData[3];

		if( totalData < dataSize )
			return;

		/*qDebug() << "PACKET TYPE > " << packetType;
		qDebug() << "DATA SIZE > "	 << dataSize;
		qDebug() << "INPUT DATA BUFFER SIZE > "	 << totalData;*/

		switch( packetType )
		{
		case PACKET_GET_UPDATES:
			{
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );

				onGetUpdates();

				//	Can process some more data.
				if( m_inputData.size() > 4 )
					onReadyRead();
			}
			break;
		case PACKET_SCREEN_INFO:
			{
				QBuffer	reader( &m_inputData );
				reader.open( QIODevice::ReadOnly );
				reader.seek( 4 );

				QByteArray	screenInfo	= reader.read( dataSize  );
				onScreenInfo( screenInfo );

				reader.close();
				
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );
			}
			break;
		case PACKET_SCREEN_CHUNK:
			{
				QBuffer	reader( &m_inputData );
				reader.open( QIODevice::ReadOnly );
				reader.seek( 4 );

				QByteArray	screenInfo	= reader.read( dataSize  );
				onScreenChunk( screenInfo );

				reader.close();
				
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );

				//	Can process some more data.
				if( m_inputData.size() > 4 )
					onReadyRead();
			}
			break;
		case PACKET_SCREEN_UPDATE:
			{
				QBuffer	reader( &m_inputData );
				reader.open( QIODevice::ReadOnly );
				reader.seek( 4 );

				QByteArray	screenInfo	= reader.read( dataSize  );
				onScreenChunk( screenInfo );

				reader.close();
				
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );

				//	Can process some more data.
				if( m_inputData.size() > 4 )
					onReadyRead();
			}
			break;
		case PACKET_SIMULATE_EVENT:
			{
				QBuffer	reader( &m_inputData );
				reader.open( QIODevice::ReadOnly );
				reader.seek( 4 );

				QByteArray	eventInfo	= reader.read( dataSize  );
				onEventInfo( eventInfo );

				reader.close();
				
				//	Remove what we just read.
				m_inputData.remove( 0, 4 + dataSize );

				//	Can process some more data.
				if( m_inputData.size() > 4 )
					onReadyRead();
			}
			break;
		default:
			assert( false );
		}
	}
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onError( QAbstractSocket::SocketError socketError )
{
	qDebug() << "SESSION COMMUNICATOR SOCKET ERROR > " << socketError;
}
////////////////////////////////////////////////////////////////////////////////

void	SessionCommunicator::onSendEvents()
{
	send();
}
////////////////////////////////////////////////////////////////////////////////