#ifndef __SESSIONCOMMUNICATOR_H__
#define __SESSIONCOMMUNICATOR_H__

////////////////////////////////////////////////////////////////////////////////

#include "statemachine/sessionstate.h"

#include <QObject>
#include <QHostAddress>
#include <QAbstractSocket>

////////////////////////////////////////////////////////////////////////////////

class	Session;
class	QTcpServer;
class	QTcpSocket;

////////////////////////////////////////////////////////////////////////////////

class SessionCommunicator : public QObject
{
	Q_OBJECT

public:
	SessionCommunicator();
	~SessionCommunicator();

	void	start();
	void	onStart();
	void	connectToHost( const QHostAddress& address, quint16 port );
	void	pushData( const QByteArray& data );
	void	sendData();
	void	send();
	void	notifyGuiConnected();

    void    changeState( SessionState* state );

	QHostAddress	getPeerAddress()		const;
	QTcpServer&		getConnectionServer()	const;
	QTcpSocket&		getConnectionSocket()	const;
	
	void	onNewConnection();

signals:
	
	void	enableTabs();
	void	connectionEstablished();

public	slots:
	
	void	onConnected();
	void	onDisconnected();
	void	onReadyRead();
	void	onError( QAbstractSocket::SocketError socketError );
	void	onSendEvents();

private:

	void	startListen();
	void	onScreenInfo( const QByteArray& screenInfo );
	void	onEventInfo( const QByteArray& eventInfo );
	void	onScreenChunk( const QByteArray& screenData );
	void	onGetUpdates();

private:
	
	QTcpServer*	m_server;
	QTcpSocket*	m_socket;

	QList<QByteArray>	m_data;
	QByteArray			m_inputData;
	QHostAddress		m_peerAddress;

    SessionState*		m_sessionState;
};
////////////////////////////////////////////////////////////////////////////////

inline
void	SessionCommunicator::pushData( const QByteArray& data )
{
	m_data.append( data );
}
////////////////////////////////////////////////////////////////////////////////

inline
void	SessionCommunicator::changeState( SessionState* state )
{
	m_sessionState	= state;
	m_sessionState->handle();
}
////////////////////////////////////////////////////////////////////////////////

inline
QHostAddress	SessionCommunicator::getPeerAddress()	const
{
	return	m_peerAddress;
}
////////////////////////////////////////////////////////////////////////////////

inline
QTcpServer&	SessionCommunicator::getConnectionServer()	const
{
	return	*m_server;
}
////////////////////////////////////////////////////////////////////////////////

inline
QTcpSocket&	SessionCommunicator::getConnectionSocket()	const
{
	return	*m_socket;
}
////////////////////////////////////////////////////////////////////////////////

#endif // __SESSIONCOMMUNICATOR_H__
