
/*****************************************************************************
FILE		: sessionthread.cpp
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sessionthread.h"

#include "../../core.h"
#include "../session.h"
#include "../sessioncommunicator.h"
#include "../capture/capturethread.h"
#include "../../globaldefinitions.h"

#include <QHostAddress>

////////////////////////////////////////////////////////////////////////////////

SessionThread::SessionThread( bool isHost, const QHostAddress& hostAddress, const QSize& desktopSize ) : QThread(),
    m_session( NULL ),
    m_sessionState( NULL ),
	m_communicator( NULL ),
	m_host( isHost ),
	m_hostAddress( hostAddress ),
	m_desktopSize( desktopSize )
{

}
////////////////////////////////////////////////////////////////////////////////

SessionThread::~SessionThread()
{
	/*delete	m_session;
	delete	m_sessionState;*/
}
////////////////////////////////////////////////////////////////////////////////

void	SessionThread::changeState( SessionState* state )
{
	m_sessionState = state;

	if( isRunning() )
	{
		process();
	}
}
////////////////////////////////////////////////////////////////////////////////

void	SessionThread::run()
{
    //while( isRunning() )
    {
		process();
		exec();
    }
}
////////////////////////////////////////////////////////////////////////////////

void	SessionThread::process()
{
    m_session	= new	Session( m_host );
	m_session->setScreenSize( m_desktopSize );

	if( ! m_host )
		m_session->setHostAddress( m_hostAddress );

	m_communicator	= new SessionCommunicator();
	connect( m_communicator, SIGNAL(connectionEstablished()), m_session, SLOT(onConnectionEstablished()), Qt::QueuedConnection );
	connect( m_communicator, SIGNAL(enableTabs()), g_Core->getMainWindow(), SLOT(onEnableTabs()), Qt::QueuedConnection );
	
	if( m_host )
		connect( g_Core->getCaptureThread(), SIGNAL(newScreenUpdate()), m_session, SLOT(onSendUpdates()), Qt::QueuedConnection );
	
	m_communicator->start();
}
////////////////////////////////////////////////////////////////////////////////
