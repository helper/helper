
#ifndef __SESSIONTHREAD_H__
#define __SESSIONTHREAD_H__

/*****************************************************************************
FILE		: sessionthread.h
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include <QSize>
#include <QThread>
#include <QHostAddress>

////////////////////////////////////////////////////////////////////////////////

class	Session;
class	SessionState;
class	SessionCommunicator;

////////////////////////////////////////////////////////////////////////////////

class SessionThread : public QThread
{

public:
    SessionThread( bool isHost, const QHostAddress& hostAddress, const QSize& desktopSize );
    ~SessionThread();

    Session*	getSession()	const;
	SessionCommunicator*	getCommunicator()	const;
	
    void    process();
    void    changeState( SessionState* state );

protected:
    void    run();

private:
	bool			m_host;
	QSize			m_desktopSize;
	QHostAddress	m_hostAddress;

    Session*	    m_session;
    SessionState*   m_sessionState;

	SessionCommunicator*	m_communicator;
};
////////////////////////////////////////////////////////////////////////////////

inline
Session*    SessionThread::getSession()    const
{
    return  m_session;
}
////////////////////////////////////////////////////////////////////////////////

inline
SessionCommunicator*	SessionThread::getCommunicator()	const
{
	return m_communicator;
}
////////////////////////////////////////////////////////////////////////////////

#endif // __SESSIONTHREAD_H__
