
/*****************************************************************************
FILE		: stateconnected.cpp
DESCRIPTION	: State class used when peer is connected to the host.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "stateconnected.h"

#include "../session.h"
#include "../../core.h"
#include "sessionthread.h"
#include "../sessioncommunicator.h"

#include <QTcpSocket>

////////////////////////////////////////////////////////////////////////////////

StateConnected*    StateConnected::m_instance	= NULL;

////////////////////////////////////////////////////////////////////////////////

StateConnected::StateConnected()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
StateConnected*	StateConnected::instance()
{
    if( m_instance == NULL )
		m_instance  = new StateConnected();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	StateConnected::onDisconnected()
{
	g_Core->getSessionThread()->getCommunicator()->onDisconnected();
}
////////////////////////////////////////////////////////////////////////////////

void	StateConnected::onReadyRead()
{
	g_Core->getSessionThread()->getCommunicator()->onReadyRead();
}
////////////////////////////////////////////////////////////////////////////////

void	StateConnected::onError( QAbstractSocket::SocketError socketError )
{
	g_Core->getSessionThread()->getCommunicator()->onError( socketError );
}
////////////////////////////////////////////////////////////////////////////////

void	StateConnected::handle()
{
    SessionCommunicator*  communicator	= g_Core->getSessionThread()->getCommunicator();
	communicator->notifyGuiConnected();
	communicator->onConnected();
	
	QTcpSocket&	socket	= communicator->getConnectionSocket();

	connect( &socket, SIGNAL(disconnected()),						SLOT(onDisconnected())		);
	connect( &socket, SIGNAL(readyRead()),							SLOT(onReadyRead())			);
	connect( &socket, SIGNAL(error(QAbstractSocket::SocketError)),	SLOT(onError(QAbstractSocket::SocketError)) );
}
////////////////////////////////////////////////////////////////////////////////