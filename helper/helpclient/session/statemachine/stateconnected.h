
#ifndef __STATECONNECTED_H__
#define __STATECONNECTED_H__

/*****************************************************************************
FILE		: stateconnected.h
DESCRIPTION	: State class used when peer is connected to the host.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sessionstate.h"

#include <QObject>
#include <QAbstractSocket>

////////////////////////////////////////////////////////////////////////////////

class StateConnected : public QObject, public SessionState
{
	Q_OBJECT

public:
    StateConnected();

    static  StateConnected*   instance();

private slots:
	
	void	onDisconnected();
	void	onReadyRead();
	void	onError( QAbstractSocket::SocketError socketError );

protected:
    virtual void    handle();

private:
    static  StateConnected*   m_instance;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __STATECONNECTED_H__
