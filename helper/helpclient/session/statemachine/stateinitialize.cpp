
/*****************************************************************************
FILE		: stateinitialize.cpp
DESCRIPTION	: State class for initializing session state.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "stateinitialize.h"

#include "../../core.h"
#include "../session.h"
#include "sessionthread.h"
#include "stateconnected.h"
#include "statenewconnection.h"
#include "../sessioncommunicator.h"

#include <QTcpServer>
#include <QTcpSocket>

////////////////////////////////////////////////////////////////////////////////

StateInitialize*    StateInitialize::m_instance	= NULL;

////////////////////////////////////////////////////////////////////////////////

StateInitialize::StateInitialize() : QObject(), SessionState()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
SessionState*	StateInitialize::instance()
{
    if( m_instance == NULL )
		m_instance  = new StateInitialize();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	StateInitialize::onConnected()
{
	SessionCommunicator*	communicator	= g_Core->getSessionThread()->getCommunicator();

	//	Go to next state.
	communicator->changeState( StateConnected::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	StateInitialize::onNewConnection()
{
	SessionCommunicator*	communicator	= g_Core->getSessionThread()->getCommunicator();

	//	Go to next state.
	communicator->changeState( StateNewConnection::instance() );
}
////////////////////////////////////////////////////////////////////////////////

void	StateInitialize::handle()
{
	bool	isPresenter	= g_Core->getSessionThread()->getSession()->isPresenter();

	SessionCommunicator*	communicator	= g_Core->getSessionThread()->getCommunicator();
	communicator->onStart();

	if( isPresenter )
	{
		//	Handle incoming connections.
		connect( &communicator->getConnectionServer(), SIGNAL(newConnection()), SLOT(onNewConnection()) );
	}
	else
	{
		//	Handle outgoinh connections.
		connect( &communicator->getConnectionSocket(), SIGNAL(connected()), SLOT(onConnected()) );
	}
}
////////////////////////////////////////////////////////////////////////////////