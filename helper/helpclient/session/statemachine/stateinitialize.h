
#ifndef __STATEINITIALIZE_H__
#define __STATEINITIALIZE_H__

/*****************************************************************************
FILE		: stateinitialize.h
DESCRIPTION	: State class for initializing session state.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include <QObject>
#include "sessionstate.h"

////////////////////////////////////////////////////////////////////////////////

class StateInitialize	: public QObject, public SessionState
{
	Q_OBJECT

public:
    StateInitialize();

    static  SessionState*   instance();

public	slots:

	void	onConnected();
	void	onNewConnection();

protected:
    virtual void    handle();

private:
    static  StateInitialize*	m_instance;

};

////////////////////////////////////////////////////////////////////////////////

#endif // STATEINITIALIZE_H
