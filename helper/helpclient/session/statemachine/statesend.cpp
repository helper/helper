
/*****************************************************************************
FILE		: statesend.cpp
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "statesend.h"

#include "../../core.h"
#include "sessionthread.h"
#include "../sessioncommunicator.h"

////////////////////////////////////////////////////////////////////////////////

StateSend*   StateSend::m_instance    = NULL;

////////////////////////////////////////////////////////////////////////////////

StateSend::StateSend()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
StateSend*   StateSend::instance()
{
    if( m_instance == NULL )
		m_instance  = new StateSend();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	StateSend::handle()
{	
	SessionCommunicator*	communicator	= g_Core->getSessionThread()->getCommunicator();
	communicator->send();
	//Session*	    session = thread->getSession();
	//bool			canRead = session->onWaitForRead();
	//
	//while( ! canRead )
	//{
	//	bool	sendUpdates		= ( session->isPresenter() && session->hasUpdates() );
	//	//bool	sendKeyEvents	= ( ! session->isPresenter() && session->hasKeyEvents() );

	//	if( sendUpdates )
	//		session->sendUpdates();
	//
	//	/*if( sendKeyEvents )
	//		session->sendKeyEvents();*/

	//	canRead = session->onWaitForRead();
	//}

	//if( canRead )
	//	thread->changeState( StateStreaming::instance() );
}
////////////////////////////////////////////////////////////////////////////////