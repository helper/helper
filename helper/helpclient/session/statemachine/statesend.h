
#ifndef __STATESEND_H__
#define __STATESEND_H__

/*****************************************************************************
FILE		: statesend.h
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include "sessionstate.h"

////////////////////////////////////////////////////////////////////////////////

class StateSend : public SessionState
{
public:
    StateSend();

    static  StateSend*	instance();

protected:

    virtual void    handle();

private:

    static  StateSend*	m_instance;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __STATESEND_H__
