
#ifndef __STATESTREAMING_H__
#define __STATESTREAMING_H__

/*****************************************************************************
FILE		: stateread.h
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include "sessionstate.h"

////////////////////////////////////////////////////////////////////////////////

class StateStreaming : public SessionState
{
public:
    StateStreaming();

    static  StateStreaming*	instance();

protected:

    virtual void    handle();

private:

    static  StateStreaming*	m_instance;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __STATESTREAMING_H__
