
/*****************************************************************************
FILE		: statewaitforread.cpp
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "statewaitforread.h"

#include "stateread.h"
#include "../session.h"
#include "../../core.h"
#include "sessionthread.h"

////////////////////////////////////////////////////////////////////////////////

StateWaitForRead*   StateWaitForRead::m_instance    = NULL;

////////////////////////////////////////////////////////////////////////////////

StateWaitForRead::StateWaitForRead()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
StateWaitForRead*   StateWaitForRead::instance()
{
    if( m_instance == NULL )
		m_instance  = new StateWaitForRead();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	StateWaitForRead::handle()
{
    SessionThread*  thread = g_Core->getSessionThread();

	if( thread != NULL )
	{
		Session*	    session = thread->getSession();
		bool			canRead = session->onWaitForRead();
		
		while( ! canRead )
		{
			bool	sendUpdates		= ( session->isPresenter() && session->hasUpdates() );
			//bool	sendKeyEvents	= ( ! session->isPresenter() && session->hasKeyEvents() );

			if( sendUpdates )
				session->sendUpdates();
		
			/*if( sendKeyEvents )
				session->sendKeyEvents();*/

			canRead = session->onWaitForRead();
		}

		//if( canRead )
			//thread->changeState( StateRead::instance() );
	}
}
////////////////////////////////////////////////////////////////////////////////