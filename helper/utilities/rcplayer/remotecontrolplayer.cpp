
/*****************************************************************************
FILE		: remotecontrolplayer.cpp
DESCRIPTION	: Multiplatform remote control player.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include "remotecontrolplayer.h"

#ifdef PLATFORM_WIN32
#include "remotecontrolplayer_win.h"
#endif

////////////////////////////////////////////////////////////////////////////////

RemoteControlPlayer::RemoteControlPlayer() : m_d( 0 )
{
	m_d	= new RemoteControlPlayerBase();
}
////////////////////////////////////////////////////////////////////////////////

RemoteControlPlayer::~RemoteControlPlayer()
{
	if( m_d != 0 )
	{
		delete	m_d;
		m_d	= 0;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayer::playKeyPressed( int virtualKey )
{
	m_d->playKeyPressed( virtualKey );
}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayer::playKeyReleased( int virtualKey )
{
	m_d->playKeyReleased( virtualKey );
}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayer::playMousePressed( bool left )
{
	m_d->playMousePressed( left );
}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayer::playMouseReleased( bool left )
{
	m_d->playMouseReleased( left );
}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayer::playMouseMove( int x, int y )
{
	m_d->playMouseMove( x, y );
}
///////////////////////////////////////////////////////////////////////////////