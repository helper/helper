#ifndef __REMOTECONTROLPLAYER_H__
#define __REMOTECONTROLPLAYER_H__

/*****************************************************************************
FILE		: remotecontrolplayer.h
DESCRIPTION	: Multiplatform remote control player.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

class	RemoteControlPlayerBase;

////////////////////////////////////////////////////////////////////////////////

class RemoteControlPlayer
{
public:
	RemoteControlPlayer();
	~RemoteControlPlayer();

	void	playKeyPressed( int virtualKey );
	void	playKeyReleased( int virtualKey );
	void	playMousePressed( bool left );
	void	playMouseReleased( bool left );
	void	playMouseMove( int x, int y );

private:
	RemoteControlPlayerBase*	m_d;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __REMOTECONTROLPLAYER_H__
