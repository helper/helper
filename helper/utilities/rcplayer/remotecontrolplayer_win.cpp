
/*****************************************************************************
FILE		: remotecontrolplayer_win.cpp
DESCRIPTION	: Base remote control player for the Windows OS.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "remotecontrolplayer_win.h"

#include "Windows.h"

////////////////////////////////////////////////////////////////////////////////

RemoteControlPlayerBase::RemoteControlPlayerBase()
{

}
////////////////////////////////////////////////////////////////////////////////

RemoteControlPlayerBase::~RemoteControlPlayerBase()
{

}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayerBase::playKeyPressed( int virtualKey )
{
	bool		extend	= false;
	INPUT		input	= { 0 };
	KEYBDINPUT	kb		= { 0 };

	switch( virtualKey )
	{
	case	VK_DELETE:
	case	VK_HOME:
	case	VK_LEFT:
	case	VK_UP:
	case	VK_RIGHT:
	case	VK_DOWN:
	case	VK_PRIOR:
	case	VK_NEXT:
	case	VK_END:
	case	VK_INSERT:
	case	VK_CANCEL:
	case	VK_RETURN:
	case	VK_DIVIDE:
	case	VK_RCONTROL:
	case	VK_RMENU:
	case	VK_PRINT:

		extend	= true;
		break;
	}

	if ( extend )
		kb.dwFlags  = KEYEVENTF_EXTENDEDKEY;

	kb.wVk		= virtualKey;
		 
	input.type  = INPUT_KEYBOARD;
	input.ki	= kb;
		
	::SendInput( 1, &input, sizeof(input)		);
	::ZeroMemory( &kb,		sizeof(KEYBDINPUT)	);
	::ZeroMemory( &input,	sizeof(INPUT)		);
}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayerBase::playKeyReleased( int virtualKey )
{	
	bool		extend	= false;
	INPUT		input	= { 0 };
	KEYBDINPUT	kb		= { 0 };
	kb.dwFlags			=  KEYEVENTF_KEYUP;

	switch( virtualKey )
	{
	case	VK_DELETE:
	case	VK_HOME:
	case	VK_LEFT:
	case	VK_UP:
	case	VK_RIGHT:
	case	VK_DOWN:
	case	VK_PRIOR:
	case	VK_NEXT:
	case	VK_END:
	case	VK_INSERT:
	case	VK_CANCEL:
	case	VK_RETURN:
	case	VK_DIVIDE:
	case	VK_RCONTROL:
	case	VK_RMENU:
	case	VK_PRINT:

		extend	= true;
		break;
	}

	if ( extend )
		kb.dwFlags  |= KEYEVENTF_EXTENDEDKEY;

	kb.wVk		= virtualKey;
	input.type  = INPUT_KEYBOARD;
	input.ki	= kb;
		
	::SendInput( 1, &input, sizeof(input) );
}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayerBase::playMousePressed( bool left )
{
	INPUT    input		=	{ 0 };
	// left down 
	input.type			= INPUT_MOUSE;
	if( left )
		input.mi.dwFlags	= MOUSEEVENTF_LEFTDOWN;
	else
		input.mi.dwFlags	= MOUSEEVENTF_RIGHTDOWN;


	::SendInput( 1, &input, sizeof(input) );
}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayerBase::playMouseReleased( bool left )
{
	INPUT    input		=	{ 0 };
	// left up
	input.type			= INPUT_MOUSE;
	if( left )
		input.mi.dwFlags	= MOUSEEVENTF_LEFTUP;
	else
		input.mi.dwFlags	= MOUSEEVENTF_RIGHTUP;

	::SendInput( 1, &input, sizeof(INPUT) );
}
////////////////////////////////////////////////////////////////////////////////

void	RemoteControlPlayerBase::playMouseMove( int x, int y )
{
	double fScreenWidth   = ::GetSystemMetrics( SM_CXSCREEN ) - 1; 
	double fScreenHeight  = ::GetSystemMetrics( SM_CYSCREEN ) - 1;

	double fx	= x *( 65535.0f/fScreenWidth );
	double fy	= y *( 65535.0f/fScreenHeight );
	INPUT  input= { 0 };

	input.type			= INPUT_MOUSE;
	input.mi.dwFlags	= MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE;
	input.mi.dx = fx;
	input.mi.dy = fy;

	::SendInput( 1, &input, sizeof(input) );
}
////////////////////////////////////////////////////////////////////////////////
