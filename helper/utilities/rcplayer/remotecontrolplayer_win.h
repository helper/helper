#ifndef __REMOTECONTROLPLAYER_WIN_H__
#define __REMOTECONTROLPLAYER_WIN_H__

/*****************************************************************************
FILE		: remotecontrolplayer_win.h
DESCRIPTION	: Base remote control player for the Windows OS.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

class RemoteControlPlayerBase
{
public:
	RemoteControlPlayerBase();
	~RemoteControlPlayerBase();

	void	playKeyPressed( int virtualKey );
	void	playKeyReleased( int virtualKey );
	void	playMousePressed( bool left );
	void	playMouseReleased( bool left );
	void	playMouseMove( int x, int y );

private:
};

#endif // __REMOTECONTROLPLAYER_WIN_H__
