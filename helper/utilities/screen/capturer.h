
#ifndef __CAPTURER_H__
#define __CAPTURER_H__

/*****************************************************************************
FILE		: capturer.h
DESCRIPTION	: Multiplatform screen capture.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

class ScreenCapturerBase;

////////////////////////////////////////////////////////////////////////////////

class Capturer
{

public:
    Capturer();
    ~Capturer();

    void    captureDesktop( char* imageBuffer );

private:
    ScreenCapturerBase*	m_d;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __CAPTURER_H__
