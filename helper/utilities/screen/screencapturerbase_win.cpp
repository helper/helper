
/*****************************************************************************
FILE		: screencapturerbase_win.cpp
DESCRIPTION	: Base screen capturer for the Windows OS.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "screencapturerbase_win.h"

#include "assert.h"

////////////////////////////////////////////////////////////////////////////////

ScreenCapturerBase::ScreenCapturerBase()
{
    m_screenWidth	= ::GetSystemMetrics( SM_CXSCREEN );
    m_screenHeight	= ::GetSystemMetrics( SM_CYSCREEN );
    m_hDesktopWnd	= ::GetDesktopWindow();
    m_hDesktopDC	= ::GetDC( m_hDesktopWnd );
    m_hCaptureDC	= ::CreateCompatibleDC( m_hDesktopDC );
    m_hCaptureBitmap= ::CreateCompatibleBitmap( m_hDesktopDC,
						    m_screenWidth,
						    m_screenHeight );
}
////////////////////////////////////////////////////////////////////////////////

void	ScreenCapturerBase::capture( char* buffer )
{
	int	err	= ::GetLastError();
	assert( err <= 0 );
   ::SelectObject( m_hCaptureDC, m_hCaptureBitmap );
   ::BitBlt( m_hCaptureDC, 0, 0,
	     m_screenWidth, m_screenHeight,
	     m_hDesktopDC, 0, 0, SRCCOPY|CAPTUREBLT );

   // Get the BITMAP from the HBITMAP
   BITMAP   bmpScreen;
   ::GetObject( m_hCaptureBitmap,sizeof(BITMAP),&bmpScreen );

   BITMAPINFOHEADER	bi;

   bi.biSize		= sizeof( BITMAPINFOHEADER );
   bi.biWidth		= bmpScreen.bmWidth;
   bi.biHeight		= -bmpScreen.bmHeight;  // This is upside-down image, negative will make ::GetDibBits to flip it.
   bi.biPlanes		= 1;
   bi.biBitCount	= 32;
   bi.biCompression	= BI_RGB;
   bi.biSizeImage	= 0;
   bi.biXPelsPerMeter	= 0;
   bi.biYPelsPerMeter	= 0;
   bi.biClrUsed		= 0;
   bi.biClrImportant	= 0;

   // Gets the "bits" from the bitmap and copies them into a buffer
   ::GetDIBits( m_hDesktopDC, m_hCaptureBitmap, 0,
		(UINT)bmpScreen.bmHeight,
		buffer,
		(BITMAPINFO *)&bi, DIB_RGB_COLORS );
}
////////////////////////////////////////////////////////////////////////////////

