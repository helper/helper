
#ifndef __SCREENCAPTURERBASE_WIN_H__
#define __SCREENCAPTURERBASE_WIN_H__

/*****************************************************************************
FILE		: screencapturerbase_win.h
DESCRIPTION	: Base screen capturer for the Windows OS.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "Windows.h"

////////////////////////////////////////////////////////////////////////////////

class ScreenCapturerBase
{

public:
    ScreenCapturerBase();

    void    capture( char* buffer );

private:
    int	    m_screenWidth;
    int	    m_screenHeight;

    HWND	    m_hDesktopWnd;
    HDC		    m_hDesktopDC;
    HDC		    m_hCaptureDC;
    HBITMAP	    m_hCaptureBitmap;
};
////////////////////////////////////////////////////////////////////////////////

#endif // SCREENCAPTURERBASE_WIN_H
