# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

# This is a reminder that you are using a generated .pro file.
# Remove it when you are finished editing this file.
message("You are running qmake on a generated .pro file. This may not work!")


HEADERS += ./screen/capturer.h \
    ./screen/screencapturerbase_win.h \
    ./rcplayer/remotecontrolplayer.h \
    ./rcplayer/remotecontrolplayer_win.h
SOURCES += ./screen/capturer.cpp \
    ./screen/screencapturerbase_win.cpp \
    ./rcplayer/remotecontrolplayer.cpp \
    ./rcplayer/remotecontrolplayer_win.cpp
